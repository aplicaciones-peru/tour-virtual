let config = {
	target: 'serverless',
	dynamicAssetPrefix: true,
	hmr: false
}
module.exports = config
