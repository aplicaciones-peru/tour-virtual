import styles from './layout.module.css'
import Link from 'next/link'
//Layout
class Layout extends React.Component {
  constructor(props) {
    super(props);
    this.state = {menuOpen:false}
  }
  onPressMenu(){
    this.setState({menuOpen:!this.state.menuOpen});
  }
  ////document.documentElement.style.setProperty('--vh', vh+'px');
  render() {
    return (<div id="pagina">
      <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;500;600;700&display=swap" rel="stylesheet"/>
      <header id="header">
        <div className="header_container">
        <Link href="/"><div className="logo"></div></Link>
        <div className="top_btn_content">
        <a href="https://www.upn.edu.pe/" target="_blank" className="top_btn">Más información</a>
        <a href="https://www.upn.edu.pe/postula-aqui" target="_blank" className="top_btn">POSTULA AQUÍ</a>
        </div>
        { !this.state.menuOpen &&
          <div className="menu" onClick={()=>{this.onPressMenu()}}></div>
        }
        { this.state.menuOpen &&
          <div className="menu_close" onClick={()=>{this.onPressMenu()}}></div>
        }
        </div>
        <script dangerouslySetInnerHTML={{ __html: `
          let vh = window.innerHeight * 0.01 + 'px';
          document.documentElement.style.setProperty('--vh', vh);
          window.addEventListener('resize', function () {
            var vh = window.innerHeight * 0.01 + 'px';
            document.documentElement.style.setProperty('--vh', vh);
          });
  			` }} />
      </header>
      <main id="main">
        { this.state.menuOpen &&
          <div className="mobile_list_menu">
            <ul>
            <li><a href="#">INICIO</a></li>
            <li><a href="#">NUESTROS CAMPUS</a></li>

            <ul className="list_campus">
            <li><Link href="/campus/brena"><a>> Breña</a></Link></li>
            <li><Link href="/campus/chorrillos"><a>> Chorrillos</a></Link></li>
            <li><Link href="/campus/comas"><a>> Comas</a></Link></li>
            <li><Link href="/campus/losolivos"><a>> Los Olivos</a></Link></li>
            <li><Link href="/campus/sanjuandelurigancho"><a>> San Juan de Lurigancho</a></Link></li>
            <li><Link href="/campus/trujillo_elmolino"><a>> Trujillo - El Molino</a></Link></li>
            <li><Link href="/campus/trujillo_sanisidro"><a>> Trujillo - San Isidro</a></Link></li>
            <li><Link href="/campus/cajamarca"><a>> Cajamarca</a></Link></li>
            </ul>

            </ul>
          </div>
        }
        {this.props.children}
      </main>
      <footer id="footer">
        <p>Universidad Privada del Norte - Todos los derechos reservados</p>
      </footer>
    </div>);
  }
}
//export default Layout({ children })
export default Layout;
