import styles from './pop.module.css'
import Map from './map'
import { useRouter } from 'next/router'
import YouTube from 'react-youtube'
//{this.state.tipo="texto" &&
//          {this.props.tipo="texto" &&
class ListElement extends React.Component {
  constructor(props) {
    super(props);
      this.state = {
        over:(Number(this.props.currentAmb)==Number(this.props.id))?true:false,
        current:(Number(this.props.currentAmb)==Number(this.props.id))?true:false,
        show:this.props.show
      }
      this.onMouseOver = this.onMouseOver.bind( this );
      this.onMouseLeave = this.onMouseLeave.bind( this );
      this.onClick = this.onClick.bind( this );
      this.onClickArea = this.onClickArea.bind( this );

      if(Number(this.props.currentAmb)==Number(this.props.id)){

      }
      this.elemento = React.createRef();
  }
  onClickArea(){
    this.props.onClickArea(this.props.facultad);
  }
  show(){
    this.setState({show:true});
  }
  hide(){
    this.setState({show:false});
  }
  onMouseOverE() {
    this.setState({over:true});
  }
  onMouseLeaveE(){
    if(!this.state.current){
      this.setState({over:false});
    }
  }
  onMouseOver() {
    this.props.onMouseOver(this.props.id);
    this.setState({over:true});
  }
  onClick(){
    this.props.onClick(this.props.id);
  }
  onMouseLeave(){
    this.props.onMouseLeave(this.props.id);
    if(!this.state.current){this.setState({over:false});}
  }
  componentDidMount(){
    if(this.state.current){
      this.refs.elemento.scrollIntoView({ behavior: 'smooth', block: 'start' });
    }
  }
  render(){
    var style = "";
    if(this.state.current){
      style=styles.icoCurrentSelected;
    }else{
      style=(this.state.over)?styles.icoLeftSelected:'';
    }

    return (<div>
              { this.props.facultad_label!=undefined &&
                <div className={styles.facultad} onClick={this.onClickArea}>> {this.props.facultad_label}</div>
              }
              { this.state.show &&
              <li ref="elemento" className={style} onClick={this.onClick} onMouseOver={this.onMouseOver} onMouseLeave={this.onMouseLeave}>
      <span className={styles.icoLeft}>> </span>{this.props.texto}
              </li> }
              { !this.state.show &&
                <div ref="elemento"></div>
              }

            </div>);
  }
}

class Pop extends React.Component {
  constructor(props) {
    super(props);
    this.mapa = React.createRef();
    this.state = {tipo:props.tipo,currentAmb:this.props.current,openFacultad:-1};
    this.onClickArea = this.onClickArea.bind( this );
  }
  onListOver(id){
    this.refs['item'+id].onMouseOverE();
  }
  onListOverE(id){
    this.refs['item'+id].refs['elemento'].scrollIntoView({ behavior: 'smooth', block: 'start' });
  }
  outListOver(id){
    this.refs['item'+id].onMouseLeaveE();
  }
  onAmbienteOver(id){

    this.refs.mapa.overList(id);
  }
  onAmbienteOut(id){
    this.refs.mapa.outList(id);
  }
  onClickAmbiente(id){
    this.props.onChangeAmbiente(id);
  }
  _onReady(event) {
    //event.target.pauseVideo();
  }
  onClickArea(facultad){
    var open = true;
    if(this.state.openFacultad==facultad){
      this.setState({openFacultad:-1});
      open = false;
    }else{
      this.setState({openFacultad:facultad});
    }
    for(var i=0;i<this.props.data.length;i++){
      this.refs['item'+(i+1)].hide();
      this.refs.mapa.hide((i+1));
    }
    for(var i=0;i<this.props.data.length;i++){
      if(this.props.data[i].facultad==facultad){
        if(open){
          this.refs['item'+(i+1)].show();
          this.refs.mapa.show((i+1));
        }else{
          this.refs['item'+(i+1)].hide();
          this.refs.mapa.hide((i+1));
        }

      }
    }
  }
  render() {
    const opts = {
      height: '390',
      width: '100%',
      playerVars: { autoplay: 1 },
    };

    var style_alt_mapa = "";
    if(this.state.currentAmb!=1){
      style_alt_mapa = styles.container_mapa_alt;
    }
    var count_current_facultad = 0;
    var current_facultad = 0;
    if(this.props.data!=undefined){
      current_facultad = this.props.data[this.state.currentAmb-1].facultad;

      /*
      for(var i=0;i<this.props.data.length;i++){
        if(this.props.data[i].facultad==current_facultad){
          console.log("----i---- "+i+" "+current_facultad);
            //this.refs['item'+(i+1)].show();
            //this.refs.mapa.show((i+1));
        }
      }
      */

      if(current_facultad!=undefined){
        var current_facultad = Number(this.props.data[this.state.currentAmb-1].facultad);
        for(var i=0;i<this.props.data.length;i++){
          if(this.props.data[i].facultad!=undefined){
              if(this.props.data[i].facultad==current_facultad){
                count_current_facultad++;
              }
          }
        }
      }
    }
    var carreras = new Array();
    if(this.props.carreras!=undefined){
      var a_carreras = this.props.carreras.split(",");
      for(var i=0;i<a_carreras.length;i++){
        carreras.push(a_carreras[i]);
      }
    }

    return (
      <div className={(this.state.tipo=="mapa"?(styles.container_mapa+' '+style_alt_mapa):styles.container)}>
          <div className={styles.header}>
            <div className={styles.iconClose} onClick={()=>{this.props.close();}}></div>
          </div>
          {this.state.tipo=="texto" &&
          <div className={styles.contentText}>
          {this.props.texto}</div>}
          {this.state.tipo=="carreras" &&
          <div className={styles.contentText} style={{marginBottom:20}}>
          <div className={styles.titleCarreras}>Carreras que aplican:</div>
          {carreras.map((value, index) => {
            return <div>{value}</div>
          })}
          </div>}
          {this.state.tipo=="video" &&
          <div className={styles.contentText}>
          <YouTube videoId={this.props.video} opts={opts} onReady={this._onReady} />
          </div>}
          {this.state.tipo=="imagen" &&
          <div className={styles.contentText}>
              {(this.props.imagen!=null && this.props.imagen!=undefined && this.props.imagen!="") &&
              <img src={"/img/campus/"+this.props.folder+"/"+this.props.imagen} />
              }
              <div className={styles.textoPop}>{this.props.texto}</div>
          </div>}
          {this.state.tipo=="mapa" &&
          <div className={styles.contentPop}>
            <div className={styles.tituloPop}>Lista de espacios</div>
            <div className={styles.contentPopLeft}>
              <Map currentAmb={this.state.currentAmb}
                    ref="mapa"
                    data={this.props.data}
                    facultad={current_facultad}
                    folder={this.props.folder}
                    nro_mapa = {this.props.nro_mapa}
                    className={styles.map}
                    onClick={(id)=>{ this.onClickAmbiente(id);}}
                    overItem={(id)=>{ this.onListOver(id);}}
                    overItemE={(id)=>{ this.onListOverE(id);}}
                    outItem={(id)=>{this.outListOver(id);}}>
              </Map>

              { (current_facultad && count_current_facultad>1) &&

                <div className={styles.btnTourAutomatico} onClick={()=>{this.props.onTourFacultad(current_facultad);}}>
                  <div className={styles.icoPlay}></div> Realizar tour automático de toda la facultad.
                </div>
              }

            </div>
            <div className={styles.contentPopRight}>
              <div className={styles.contentAmbientes}>
                <h4>AMBIENTES</h4>
                <div className={styles.listadoAmbientes}>
                  <ul ref={"listado"}>
                  {this.props.data.map((value, index) => {
                    return <ListElement
                    currentAmb={this.state.currentAmb}
                    ref={"item"+(index+1)}
                    key={index}
                    show={(value.facultad==current_facultad)?true:false}
                    id={index+1}
                    texto={value.ambiente}
                    facultad={value.facultad}
                    facultad_label={value.facultad_label}
                    onMouseOver={(i)=>{this.onAmbienteOver(i)}}
                    onMouseLeave={(i)=>{this.onAmbienteOut(i)}}
                    onClick={(i)=>{this.onClickAmbiente(i)}}
                    onClickArea={this.onClickArea}
                    />
                  })}
                  </ul>
                </div>
              </div>
            </div>
          </div>}
        </div>
    );
  }
}
//export default Pop;

export default Pop


/*
export default function Pop({ children }) {
  return <div className={styles.container}>
      <div className={styles.header}>
        <div className={styles.iconClose}></div>
      </div>
      {this.state.tipo="texto" &&
      <div className={styles.contentText}>
      Somos una universidad licenciada por SUNEDU. Contamos con la acreditación internacional IAC-SINDA y con programas acreditados por ICACIT y SINEACE. Ademas, tenemos alianza exclusiva con CNN y estamos respaldados internacionalmente por QS Stars.
      </div>
      }

  </div>
}
*/
