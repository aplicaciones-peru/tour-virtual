import styles from './visor360.module.css'
var classNames = require('classnames');
export default function Visor360({ children }) {

  var liClasses = classNames({
      'icono': true,
    });
  return <div className={styles.container}>
      <div className={styles.icono+ ' ' + styles.iconoLeft}><div></div></div>
      <div className={styles.icono+ ' ' + styles.iconoRight}><div></div></div>
      <div className={styles.icono+ ' ' +styles.iconoVerVideo}><div></div></div>
      <div className={styles.icono+ ' ' +styles.iconoVerInformacion}><div></div></div>
      <div className={styles.mainControl}>
        <div className={styles.icono+ ' ' +styles.iconoVerCampus}><div></div></div>
        <div className={styles.icono+ ' ' +styles.iconoPlay}><div></div></div>
        <div className={styles.icono+ ' ' +styles.iconoVerUbicacion}><div></div></div>
      </div>
  </div>
}
