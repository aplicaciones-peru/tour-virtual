import Layout from './layout'
import Pop from './pop'
import Link from 'next/link'
import styles from './campus.module.css'
import ReactPannellum, { onPanoramaLoaded,isLoaded,loadScene,getAllScenes,addScene,startAutoRotate,stopAutoRotate }  from "react-pannellum";
var timer=null;
class Campus extends React.Component {
  constructor(props) {
    super(props);
    this.panellum = React.createRef();
    this.state = {
        showInfo:false,
        showAmbientes:false,
        showButtons:true,
        showFoto:false,
        showCarreras:false,
        cAmb:1,
        cAmbFacultad:1,
        pitch:-9,
        data:this.props.data[0],
        facultad:new Array(),
        slideshow:false,
        slideshowTime:1000*10,
        slideshowSpeed:-7,
        show360:false
      };
    this.onPressLeft = this.onPressLeft.bind( this );
    this.onPressRight = this.onPressRight.bind( this );

    this.onChangeAmbiente = this.onChangeAmbiente.bind( this );
    this.onStopSlideshow = this.onStopSlideshow.bind( this );
    this.onTourFacultad = this.onTourFacultad.bind( this );
  }
  componentDidMount(){
    //console.log("folder "+this.props.folder);
    for(var i=1;i<this.props.data.length;i++){
      var p = i+1;
      var yaw = 0;
      if(this.props.folder=="brena" && i==5){
        yaw = 100;
      }
      var config = {  autoLoad: true,
                      hfov:150,
                      compass: false,
                      yaw:yaw,
                      pitch:this.state.pitch,
                      showControls: false,
                      keyboardZoom:false,
                      mouseZoom:false,
                      imageSource:"/img/360/"+this.props.folder+"/"+this.props.fotoname+"_"+p+".jpg" };
      var escena = "escena"+p;
      addScene(escena,config,()=>{ });
    }
  }
  onPanoramaLoaded(){
    console.log("onPanoramaLoaded1")
  }
  componentWillUnmount(){
    if(timer!=null){
      clearTimeout(timer);
    }
  }
  verInformacion(){
    this.setState({showCarreras:true,showButtons:false});
  }
  verAmbientes(){
    this.setState({showAmbientes:true,showButtons:false});
  }
  verFoto(){
    this.setState({showFoto:true,showButtons:false});
  }
  verVideo(){
    this.setState({showVideo:true,showButtons:false});
  }
  onPressLeft(){
    var current = this.state.cAmb;
    /*if(this.state.facultad.length>0){
      current = this.state.cAmbFacultad;
    }*/
    current--;
    /*if(this.state.facultad.length>0){
      this.setState({cAmb:this.props.data[current-1].id,data:this.props.data[current-1],cAmbFacultad:current});
    }else{

    }*/
    this.setState({cAmb:current,data:this.props.data[current-1]});
    var escena = "escena"+current;
    loadScene(escena);
  }
  onPressRight(){
    var current = this.state.cAmb;
    var current_facultad = this.state.cAmbFacultad;
    if(this.state.facultad.length>0){
      current_facultad++;
    }
    current++;
    if(this.state.facultad.length>0){
      this.setState({data:this.props.data[current-1],cAmb:this.props.data[current-1].id,cAmbFacultad:current_facultad});
    }else{
      this.setState({data:this.props.data[current-1],cAmb:current});
    }
    var escena = "escena"+current;
    loadScene(escena);
  }
  onChangeAmbiente(id){
    var _id = Number(id);
    var escena = "escena"+_id;
    loadScene(escena);
    this.setState({cAmb:_id,showAmbientes:false,showButtons:true,data:this.props.data[_id-1]})
  }
  onPlaySlideshow(flag){
    if(flag) this.setState({slideshow:true,facultad:new Array()});
    else this.setState({slideshow:true});
    startAutoRotate(this.state.slideshowSpeed,this.state.pitch);
    timer = setTimeout(()=>{this.timerComplete()}, this.state.slideshowTime);
  }
  timerComplete(){
    var d = this.props.data;
    var current = this.state.cAmb;
    if(this.state.facultad.length>0){
      d = this.state.facultad;
      current = this.state.cAmbFacultad;
    }
    if(current<d.length){
      this.onPressRight();
      startAutoRotate(this.state.slideshowSpeed,this.state.pitch);
      timer = setTimeout(()=>{this.timerComplete()}, this.state.slideshowTime);
    }else{
      if(this.state.facultad.length>0){
        this.setState({cAmbFacultad:1,cAmb:d[0].id,data:d[0]});
      }else{
        this.setState({cAmb:d[0].id,data:d[0]});
      }
      var escena = "escena"+d[0].id;
      loadScene(escena);
      startAutoRotate(this.state.slideshowSpeed,this.state.pitch);
      timer = setTimeout(()=>{this.timerComplete()}, this.state.slideshowTime);
    }
  }
  onStopSlideshow(){
    this.setState({slideshow:false});
    stopAutoRotate();
    clearTimeout(timer);
    timer=null;
  }
  onTourFacultad(current_facultad){
    var facultad = new Array();
    var currentFacultadCounter = 1;
    var currentFacultad = 1;
    for(var i=0;i<this.props.data.length;i++){
      if(this.props.data[i].facultad==current_facultad){
        facultad.push(this.props.data[i]);
        if(this.state.cAmb==this.props.data[i].id){
          currentFacultad=currentFacultadCounter;
        }
        currentFacultadCounter++;
      }
    }
    this.setState({showAmbientes:false,cAmbFacultad:currentFacultad,facultad:facultad},()=>{
      this.onPlaySlideshow(false);
    });

  }
  render(){
    var uiText = {loadingLabel: 'Cargando...'};
    var title_alt_style =(this.state.cAmb!=1)?styles.big:'';
    var layer_alt_style =(this.state.cAmb!=1)?styles.layer_alt:'';
    var triangulo_alt_style =(this.state.cAmb!=1)?styles.triangulo_alt:'';
    return (<Layout>
              <div className={styles.visor}>

              <div className={styles.tituloContainer}>
                <div className={styles.titulo}>
                  <div className={styles.titulo_in+' '+title_alt_style}>
                    <h1>{"Campus "+this.props.titulo}</h1>
                  { this.state.cAmb!=1 &&
                    <h2>{this.state.data.ambiente}</h2>
                  }
                  </div>
                  <div className={styles.triangulo+' '+triangulo_alt_style}>
                    <div></div>
                  </div>
                </div>
                <div className={styles.titulo_bottom}>
                  <div className={styles.titulo_bottom_yellow}></div>
                </div>
              </div>
              <div className={styles.layer1+' '+layer_alt_style}>
                <div className={styles.contenedor}>
                  <div className={styles.contenido}>

                  <ReactPannellum
                    id="1"
                    ref="panellum"
                    uiText={uiText}
                    sceneId="escena1"
                    keyboardZoom={false}
                    mouseZoom={false}
                    onPanoramaLoaded={(e)=>{
                      if(this.state.cAmb==1){
                        this.setState({show360:true});
                        setTimeout(()=>{
                          this.setState({show360:false});
                        },3000);
                      }
                    }}
                    imageSource={"/img/360/"+this.props.folder+"/"+this.props.fotoname+"_1.jpg"}
                    style = {{
                      width: "100%",
                      height: "100%",
                    }}
                    config={ {hfov:150,keyboardZoom:false,mouseZoom:false,pitch:this.state.pitch,autoLoad: true, compass: false, showControls: false,uiText:uiText}}
                  />
                  <div className={styles.overlay}>
                    { this.state.show360 &&
                      <img src={"/iconos/ico360.svg"} className={styles.ico360}/>
                    }
                    <div className={styles.overlay_grid}></div>

                  </div>
                  </div>

                  { !this.state.slideshow &&
                    <div className={styles.izquierda}>
                      { (this.state.cAmb>1) &&
                        <div className={styles.tooltip_left}>
                        <div className={styles.icono+ ' ' + styles.iconoLeft} onClick={this.onPressLeft}><div></div></div>
                        <span className={styles.tiptext}>Anterior</span>
                      </div>
                      }
                      { (this.state.cAmb==1) &&
                        <div className={styles.iL}></div>
                      }
                    </div>
                  }
                  { !this.state.slideshow &&
                  <div className={styles.derecha}>
                    { (this.state.cAmb<this.props.data.length) &&
                      <div className={styles.tooltip_right}>
                        <div className={styles.icono+ ' ' + styles.iconoRight} onClick={this.onPressRight}><div></div></div>
                        <span className={styles.tiptext}>Siguiente</span>
                      </div>
                    }
                    { (this.state.cAmb==this.props.data.length) &&
                      <div className={styles.iL}></div>
                    }
                  </div>
                  }
                  <div className={styles.accesos}>

                    { (this.state.showButtons && !this.state.slideshow) &&
                      <div className={styles.controles_video}>

                        { (this.state.data.texto!=undefined) &&
                            <div className={styles.tooltip}>
                        <div className={styles.icono+ ' ' +styles.iconoVerFoto} onClick={()=>{this.verFoto();}}><div></div></div>
                        <span className={styles.tiptext}>Información</span>
                      </div>
                        }

                        { (this.state.data.video!=undefined && this.state.data.texto!=undefined) && <div style={{width:30,backgroundColor:'transparent'}}></div>}

                        { (this.state.data.video!=undefined) &&
                          <div className={styles.tooltip}>
                            <div className={styles.icono+ ' ' +styles.iconoVerVideo} alt="Video" onClick={()=>{this.verVideo();}}><div></div></div>
                            <span className={styles.tiptext}>Video</span>
                          </div>
                        }

                        { (this.state.data.video!=undefined && this.state.data.carreras!=undefined) && <div style={{width:30,backgroundColor:'transparent'}}></div>}
                        { (this.state.data.texto!=undefined && this.state.data.carreras!=undefined) && <div style={{width:30,backgroundColor:'transparent'}}></div>}

                        { (this.state.data.carreras!=undefined) &&
                          <div className={styles.tooltip}>
                          <div className={styles.icono+ ' ' +styles.iconoVerInformacion} alt="Información" onClick={()=>{this.verInformacion();}}><div></div></div>
                          <span className={styles.tiptext}>Carreras</span>
                        </div>
                        }

                      </div>
                    }
                  </div>
                  <div className={styles.reproduccion}>
                      <div className={styles.controles}>
                        <div className={styles.mainControl}>
                          { !this.state.slideshow &&
                          <Link href="/campus">
                          <div className={styles.tooltip}>
                            <div className={styles.icono+ ' ' +styles.iconoVerCampus}><div></div></div>
                            <span className={styles.tiptext}>Campus</span>
                          </div>
                          </Link>
                          }
                          { this.state.slideshow &&
                            <div className={styles.icono+ ' ' +styles.iconoVerCampus+' '+styles.disableIcon}><div></div></div>
                          }
                          { !this.state.slideshow &&
                            <div className={styles.tooltip}>
                            <div className={styles.icono+ ' ' +styles.iconoPlay} onClick={()=>{this.onPlaySlideshow(true);}}><div></div></div>
                            <span className={styles.tiptext}>Tour automático</span>
                          </div>
                          }
                          { this.state.slideshow &&
                            <div className={styles.icono+ ' ' +styles.iconoStop} onClick={this.onStopSlideshow}><div></div></div>
                          }
                          { !this.state.slideshow &&
                              <div className={styles.tooltip}>
                              <div className={styles.icono+ ' ' +styles.iconoVerUbicacion} onClick={()=>{this.verAmbientes();}}><div></div></div>
                              <span className={styles.tiptext}>Lista de espacios</span>
                            </div>
                          }
                          { this.state.slideshow &&
                              <div className={styles.icono+ ' ' +styles.iconoVerUbicacion+ ' ' +styles.disableIcon}><div></div></div>
                          }
                        </div>
                      </div>
                  </div>
                </div>
              </div>
              <div className={styles.layer2}>

              </div>
              { this.state.showInfo &&
              <div className={styles.layer3+' '+layer_alt_style}>
                <Pop  tipo={"texto"} texto={this.state.data.info}
                      close={()=>{this.setState({showInfo:false,showButtons:true});}}>
                </Pop>
              </div>
              }
              { this.state.showCarreras &&
              <div className={styles.layer3+' '+layer_alt_style}>
                <Pop  tipo={"carreras"} carreras={this.state.data.carreras} close={()=>{this.setState({showCarreras:false,showButtons:true});}}>
                </Pop>
              </div>
              }
              { this.state.showAmbientes &&
              <div className={styles.layer3+' '+layer_alt_style}>
                <Pop  tipo={"mapa"} nro_mapa={this.props.nro_mapa}
                      onChangeAmbiente={this.onChangeAmbiente}
                      data={this.props.data}
                      current={this.state.cAmb}
                      folder={this.props.folder}
                      onTourFacultad={this.onTourFacultad}
                      close={()=>{this.setState({showAmbientes:false,showButtons:true});}}></Pop>
              </div>
              }
              { this.state.showFoto &&
              <div className={styles.layer3+' '+layer_alt_style}>
                <Pop  tipo={"imagen"} ambiente={this.state.cAmb} texto={this.state.data.texto}
                      folder={this.props.folder}
                      imagen={this.state.data.foto}
                      close={()=>{this.setState({showFoto:false,showButtons:true});}}></Pop>
              </div>
              }
              { this.state.showVideo &&
              <div className={styles.layer3+' '+layer_alt_style}>
                <Pop  tipo={"video"} video={this.state.data.video}
                      close={()=>{this.setState({showVideo:false,showButtons:true});}}

                ></Pop>
              </div>
              }
              </div>
            </Layout>);
  }

}
export default Campus;
