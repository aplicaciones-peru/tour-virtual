# UPN Tour Virtual
_Landing desarrollada para conocer los campus y ambientes de la Universidad Privada del Norte_
## Comenzando


### Pre-requisitos
* [Nodejs](https://nodejs.org/es/)
* [NPM](https://www.npmjs.com/)
* Servidor Web


### Instalación

_Clonar el proyecto en la carpeta de destino_
```
$ git clone <url git> conecta
```

_Instalar las dependencias_
```
$ npm install
```

_Compilar el proyecto_
```
$ npm run build
```

_Arrancart la aplicación_
```
$ npm run start
```

_Al acceder a la url destinada se debe mostrar la pantalla inicial de la landing_0

## Despliegue

_En caso de deplegar en un entorno Serverless_

_Renderizar la aplicacion de manera estática_
```
$ next export
```
_Copiar la carpeta `out` a la ubicación necesaria_

## Servidor
### Requisitos minimos
* RAM: 1GB
* CPU: 1
* HDD: 500mb de espacio libre

### Configuración
#### Apache
_ejemplo configuracion en apache_

```
<VirtualHost *:443>

    <Directory /sitios/upn.edu.pe/tourvirtual/>
        Options -Indexes
		AllowOverride All
        Require all granted
    </Directory>

    <Files ".ht*">
        Require all denied
    </Files>

    ServerName tourvirtual.upn.edu.pe

    DocumentRoot /sites/upn.edu.pe/tourvirtual

    SetEnvIf Request_URI "^/favicon\.ico$" dontlog
    SetEnvIf Request_URI "^/robots\.txt$" dontlog

    ErrorLog ${APACHE_LOG_DIR}/error.log
    CustomLog ${APACHE_LOG_DIR}/access.log combined

    DirectoryIndex index.php index.html

    SSLEngine on
    SSLCertificateFile "/etc/apache2/ssl/upn360/cert1920.crt"
    SSLCertificateChainFile "/etc/apache2/ssl/upn360/chain.crt"
    SSLCertificateKeyFile "/etc/apache2/ssl/upn360/key.key"
</VirtualHost>
```

*htaccess*
```.htaccess
RewriteEngine on
RewriteBase /
RewriteCond %{REQUEST_FILENAME} !-f
RewriteCond %{REQUEST_FILENAME} !-d
RewriteRule . /%{REQUEST_URI}.html [L]
```


#### NGINX
_Ejemplo configuracion en nginx_

```
server {
  listen 443 ssl tourvirtual.upn.edu.pe;

  # enables SSLv3/TLSv1, but not SSLv2 which is weak and should no longer be used.
  ssl_protocols SSLv3 TLSv1;

  # disables all weak ciphers
  ssl_ciphers ALL:!aNULL:!ADH:!eNULL:!LOW:!EXP:RC4+RSA:+HIGH:+MEDIUM;

  server_name tourvirtual.upn.edu.pe;

  ## Access and error logs.
  access_log /var/log/nginx/access.log;
  error_log  /var/log/nginx/error.log info;

  ## Keep alive timeout set to a greater value for SSL/TLS.
  keepalive_timeout 75 75;

  ssl on;
  ssl_certificate /etc/apache2/ssl/upn360/cert1920.crt;
  ssl_certificate_key /etc/apache2/ssl/upn360/key.key;
  ssl_session_timeout  5m;

  ## Strict Transport Security header for enhanced security. See
  ## http://www.chromium.org/sts. I've set it to 2 hours; set it to
  ## whichever age you want.
  add_header Strict-Transport-Security "max-age=7200";

  root /sites/upn.edu.pe/tourvirtual;
  index index.html;
  try_files $uri $uri/ /$uri.html;
}
```


## Construido con
* [Nodejs](https://nodejs.org/es/)
* [NPM](https://www.npmjs.com/)
* [Reactjs](https://es.reactjs.org/)
* [NextJs](https://nextjs.org/)
* [Bootstrap](https://getbootstrap.com/)

---
