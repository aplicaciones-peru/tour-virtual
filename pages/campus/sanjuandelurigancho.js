import Layout from '../../components/layout'
import Pop from '../../components/pop'
import Campus from '../../components/campus'
import styles from './campus.module.css'

class Sanjuandelurigancho extends React.Component {
  constructor(props) {
    super(props);
    this.state = {showInfo:false}
  }
  render(){

    var data = new Array();
    var item = {};
    item.ambiente = "FACHADA";
    item.texto = "Somos una universidad licenciada por SUNEDU. Contamos con la acreditación institucional de IAC-CINDA y con programas acreditados por ICACIT y SINEACE. Además, tenemos una alianza exclusiva con CNN y estamos respaldados internacionalmente por QS Stars.";
    item.video = "eSQr8DbQTBs";
    item.foto = "foto_sjl_1.jpg";
    item.facultad = 1;
    item.facultad_label = "Áreas Comunes";
    data.push(item);
    var item = {};
    item.ambiente = "PASADIZO PRINCIPAL";
    item.texto = "Accederás a tus clases desde blackboard, la plataforma de educación número 1 en el mundo.";
    item.foto = "foto_sjl_2.jpg";
    item.facultad = 1;
    data.push(item);
    var item = {};
    item.ambiente = "AUDITORIO";
    item.texto = "9 de cada 10 de nuestros egresados, consiguen trabajo en su primer año de egreso.";
    item.foto = "foto_sjl_3.jpg";
    item.video = "2pXewcJmdTA";
    item.facultad = 1;
    data.push(item);
    var item = {};
    item.ambiente = "TERRAZA";
    item.texto = "Podrás acceder a Empléate UPN, un programa integral donde recibes asesorías y entrenamiento virtual / Te ofrecemos Contacto UPN, una herramienta de respuesta ágil para resolver todas tus consultas.";
    item.video = "xjeeaF9ZHKU";
    item.foto = "foto_sjl_4.jpg";
    item.facultad = 1;
    data.push(item);
    var item = {};
    item.ambiente = "LOSA DEPORTIVA";
    item.texto = "Desarrollamos para ti un sistema de acompañamiento remoto con programas de nivelación, orientación personal, talleres extracurriculares y charlas.";
    item.facultad = 1;
    data.push(item);
    var item = {};
    item.ambiente = "CAFETERÍA";
    item.texto = "Somos mas de 80 mil estudiantes, interactuando con el mejor ecosistema multiplataforma de educación virtual.";
    item.foto = "foto_sjl_6.jpg";
    item.video = "vdiLD5e6A9c";
    item.facultad = 1;
    data.push(item);
    var item = {};
    item.ambiente = "CENTRO DE INFORMACIÓN";
    item.texto = "Tendrás a tu alcance mas de 380 mil materiales de lectura para complementar tu educación fisica y virtual.";
    item.foto = "foto_sjl_7.jpg";
    item.video = "Upbk6x9ZQtk";
    item.facultad = 1;
    data.push(item);

    var item = {};
    item.ambiente = "LABORATORIO DE CÓMPUTO";
    //item.texto = "9 de cada 10 de nuestros egresados, consiguen trabajo en su primer año de egreso";
    //item.video = "xjeeaF9ZHKU";
    item.facultad = 1;
    data.push(item);

    var item = {};
    item.ambiente = "AULA";
    item.texto = "";
    item.foto = "foto_aula.jpg";
    item.facultad = 1;
    data.push(item);




    var item = {};
    item.ambiente = "HOSPITAL SIMULADO";
    item.texto = "En este espacio recreamos los ambientes de internamiento de un hospital general para el entrenamiento de atención a los pacientes, usando simuladores que son capaces de reaccionar a estimulos como si fueran pacientes reales.";
    item.foto = "foto_sjl_8.jpg";
    item.facultad = 2;
    item.carreras = "Enfermería,Obstetricia,Nutrición y Dietética,Terapia Física y Rehabilitación,Psicología";
    item.facultad_label = "SALUD";
    data.push(item);
    var item = {};
    item.ambiente = "LABORATORIO DE ALTA COMPLEJIDAD";
    item.texto = "Este es un espacio simulado de una sala de emergencias, unidad de shock trauma, sala de operaciones, unidad de cuidados intensivos y/o sala de partos para nuestro entrenamiento en atención de pacientes graves usando equipos de alta complejidad.";
    item.foto = "foto_sjl_9.jpg";
    item.carreras = "Enfermería,Obstetricia,Nutrición y Dietética,Terapia Física y Rehabilitación";
    item.facultad = 2;
    data.push(item);

    var item = {};
    item.ambiente = "LABORATORIO DE ESTRUCTURA Y FUNCIÓN";
    item.texto = "En este laboratorio aprendemos sobre los órganos que conforman el cuerpo humano y el funcionamiento de los mismos mediante maquetas, aplicaciones en tablets, pintura corporal, proyección de imágenes y videos y anatomía palpatoria entre los mismos estudiantes.";
    item.foto = "foto_sjl_11.jpg";
    item.carreras = "Enfermería,Obstetricia,Nutrición y Dietética,Terapia Física y Rehabilitación,Psicología";
    item.facultad = 2;
    data.push(item);
    var item = {};
    item.ambiente = "LABORATORIO DE TERAPIA FÍSICA";
    item.texto = "Recreamos un ambiente real de terapia física y rehabilitación para el entrenamiento de las habilidades y destrezas en la atención de pacientes con limitaciones del movimiento.";
    item.foto = "foto_sjl_12.jpg";
    item.carreras = "Terapia Física y Rehabilitación";
    item.facultad = 2;
    data.push(item);




    var item = {};
    item.ambiente = "TALLER DE AUDIO";
    item.texto = " En este ambiente contamos con la capacidad para desarrollar proyectos sonoros ya sea solo musicalización, efectos, programas radiales, podcats, entrevista, entre otros.";
    item.foto = "foto_sjl_13.jpg";
    item.carreras = "Comunicación Audiovisual y Medios Digitales,Comunicación Corporativa,Comunicación y Diseño Gráfico,Comunicación y Periodismo,Comunicación y Publicidad,Comunicación";
    item.facultad = 3;
    item.facultad_label = "COMUNICACIONES";
    data.push(item);
    var item = {};
    item.ambiente = "LAB ZAM";
    item.texto = "En este ambiente contamos con un set de televisión y equipamiento necesario para preparar un producto audiovisual grabado o en vivo. Además contamos con escenografía digital para poder realizar cualquier tipo de ambientación.";
    item.foto = "foto_sjl_14.jpg";
    item.carreras = "Comunicación Audiovisual y Medios Digitales,Comunicación Corporativa,Comunicación y Diseño Gráfico,Comunicación y Periodismo,Comunicación y Publicidad,Comunicación";
    item.facultad = 3;
    data.push(item);
    var item = {};
    item.ambiente = "TALLER DE EDICIÓN";
    item.texto = "Este ambiente se encuentra equipado con computadoras especiales para edición de audio y video, como también para realizar pos-producción, titulación, musicalización, doblaje, entre otros.";
    item.facultad = 3;
    item.carreras = "Comunicación Audiovisual y Medios Digitales,Comunicación Corporativa,Comunicación y Diseño Gráfico,Comunicación y Periodismo,Comunicación y Publicidad,Comunicación";
    data.push(item);
    var item = {};
    item.ambiente = "LABORATORIO DE FOTOGRAFÍA";
    item.facultad = 3;
    item.carreras = "Comunicación Audiovisual y Medios Digitales,Comunicación Corporativa,Comunicación y Diseño Gráfico,Comunicación y Periodismo,Comunicación y Publicidad,Comunicación";
    item.texto = " Este taller cuenta con equipos de fotografía e iluminación para realizar diversos proyectos como retratos, fotos de productos, alimentos, entre otros.";
    item.foto = "foto_sjl_16.jpg";
    data.push(item);

    var item = {};
    item.ambiente = "LABORATORIO DE ELECTRÓNICA";
    item.texto = "Este ambiente cuenta con equipos de medición electrónica  para realizar prácticas de circuitos, como Osciloscopios, Generadores de ondas, Multímetros, y componentes electrónicos diversos.";
    item.foto = "foto_sjl_17.jpg";
    item.facultad = 4;
    item.carreras = "Ingeniería Electrónica ,Ingeniería Mecatrónica,Ingeniería de Sistemas Computacionales";
    item.facultad_label = "INGENIERÍA";
    data.push(item);
    var item = {};
    item.ambiente = "LABORATORIO DE CONCRETO";
    item.texto = "Este laboratorio nos permite estudiar la resistencia del concreto y otros materiales, como ladrillo, madera, acero, etc. para su correcto empleo en obras.";
    item.foto = "foto_sjl_18.jpg";
    item.carreras = "Ingeniería Civil";
    item.facultad = 4;
    data.push(item);
    var item = {};
    item.ambiente = "TALLER DE HOUSEKEEPING";
    item.texto = "Este taller recrea la habitación de un hotel con todos los implementos necesarios, y en ella podremos practicar distintas actividades del área de Housekeeping.";
    item.facultad = 5;
    item.carreras = "Administración y Servicios Turísticos";
    item.facultad_label = "NEGOCIOS";
    data.push(item);
    var item = {};
    item.ambiente = "TALLER DE COCINA";
    item.texto = "Aquí contamos con las herramientas y equipos necesarios para desarrollar técnicas culinarias, cocina nacional e internacional, panadería, pastelería, entre otros.";
    item.foto = "foto_sjl_20.jpg";
    item.facultad = 5;
    item.carreras = "Gastronomía y Gestión de Restaurantes,Administración y Servicios Turísticos";
    data.push(item);
    for(var i=0;i<data.length;i++){ data[i].id = i+1; }
    return (<Campus titulo="San Juan de Lurigancho" folder="sjl" fotoname="SJL" data={data} nro_mapa={7}></Campus>);
  }
}
export default Sanjuandelurigancho;
