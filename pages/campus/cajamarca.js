import Layout from '../../components/layout'
import Pop from '../../components/pop'
import Campus from '../../components/campus'
import styles from './campus.module.css'

class Cajamarca extends React.Component {
  constructor(props) {
    super(props);
    this.state = {showInfo:false}
  }
  render(){
      var data = new Array();
      var item = {};
      item.ambiente = "FACHADA";
      item.texto = "Somos una universidad licenciada por SUNEDU. Contamos con la acreditación institucional de IAC-CINDA y con programas acreditados por ICACIT y SINEACE. Además, tenemos una alianza exclusiva con CNN y estamos respaldados internacionalmente por QS Stars.";
      item.video = "eSQr8DbQTBs";
      item.foto = "foto_cajamarca_1.jpg";
      item.facultad = 1;
      item.facultad_label = "Áreas Comunes";
      data.push(item);
      var item = {};
      item.ambiente = "PLAZA";
      item.texto = "Accederás a tus clases desde blackboard, la plataforma de educación número 1 en el mundo.";
      item.foto = "foto_cajamarca_2.jpg";
      item.facultad = 1;
      data.push(item);
      var item = {};
      item.ambiente = "TERRAZA";
      item.texto = "Podrás acceder a Empleate UPN, un programa integral donde recibes asesorías y entrenamiento virtual / Te ofrecemos Contacto UPN, una herramienta de respuesta ágil para resolver todas tus consultas.";
      item.video = "xjeeaF9ZHKU";
      item.foto = "terraza.jpg";
      item.facultad = 1;
      data.push(item);
      var item = {};
      item.ambiente = "CAFETERÍA";
      item.texto = "Somos mas de 80 mil estudiantes, interactuando con el mejor ecosistema multiplataforma de educación virtual.";
      item.video = "vdiLD5e6A9c";
      item.foto = "foto_cajamarca_8.jpg";
      item.facultad = 1;
      data.push(item);

      var item = {};
      item.ambiente = "CAFETERÍA";
      item.texto = "Somos mas de 80 mil estudiantes, interactuando con el mejor ecosistema multiplataforma de educación virtual.";
      item.video = "vdiLD5e6A9c";
      item.foto = "foto_cajamarca_8.jpg";
      item.facultad = 1;
      data.push(item);

      var item = {};
      item.ambiente = "LOSA DEPORTIVA";
      item.texto = "Desarrollamos para ti un sistema de acompañamiento remoto con programas de nivelación, orientación personal, talleres extracurriculares y charlas.";
      item.facultad = 1;
      data.push(item);

      /*-----------------------*/
      /*-----------------------*/
      var item = {};
      item.ambiente = "AUDITORIO";
      item.foto = "foto_cajamarca_6.jpg";
      item.video = "2pXewcJmdTA";
      item.texto = "9 de cada 10 de nuestros egresados, consiguen trabajo en su primer año de egreso.";
      item.facultad = 1;
      data.push(item);

      var item = {};
      item.ambiente = "LABORATORIO DE CÓMPUTO";
      //item.texto = "9 de cada 10 de nuestros egresados, consiguen trabajo en su primer año de egreso";
      item.facultad = 1;
      data.push(item);

      var item = {};
      item.ambiente = "AULA";
      item.texto = "";
      item.foto = "foto_aula.jpg";
      item.facultad = 1;
      data.push(item);

      var item = {};
      item.ambiente = "CENTRO DE INFORMACIÓN";
      item.texto = "Tendrás a tu alcance mas de 380 mil materiales de lectura para complementar tu educación física y virtual.";
      item.video = "Upbk6x9ZQtk";
      item.foto = "foto_cajamarca_9.jpg";
      item.facultad = 1;
      data.push(item);

      /*-----------------------*/
      /*-----------------------*/


      var item = {};
      item.ambiente = "LABORATORIO DE BIOLOGÍA";
      item.texto = "En este laboratorio trabajamos con material biológico, desde nivel celular hasta el nivel de órganos y sistemas, analizándolos experimentalmente.  Contamos con equipamiento que nos permite visualizar las muestras en tiempo real y realizar mediciones microscópicas de tejidos animales y vegetales.";
      item.facultad = 2;
      item.carreras = "Ingeniería Ambiental,Ingeniería Agroindustrial,Enfermería,Obstetricia,Nutrición y Dietética,Terapia Física y Rehabilitación";
      item.facultad_label = "SALUD";
      data.push(item);

      var item = {};
      item.ambiente = "LABORATORIO DE PSICOTERAPIA (CÁMARA GESEL)";
      item.texto = "En este espacio contamos con un ambiente de practica y uno de observación. Ambos espacios cuentan con una separación que permite la visión de solo un lado para analizar y realizar diferentes experimentos sobre el comportamiento humano.";
      item.facultad = 2;
      item.carreras = "Psicología";
      data.push(item);
      var item = {};
      item.ambiente = "LAB ZAM";
      item.foto = "foto_cajamarca_12.jpg";
      item.facultad = 3;
      item.carreras = "Comunicación Audiovisual y Medios Digitales,Comunicación Corporativa,Comunicación y Diseño Gráfico,Comunicación y Periodismo,Comunicación y Publicidad,Comunicación";
      item.facultad_label = "COMUNICACIONES";
      item.texto = "En este ambiente contamos con un set de televisión y equipamiento necesario para preparar un producto audiovisual grabado o en vivo. Además contamos con escenografía digital para poder realizar cualquier tipo de ambientación.";
      data.push(item);
      var item = {};
      item.ambiente = "TALLER DE EDICIÓN";
      item.facultad = 3;
      item.carreras = "Comunicación Audiovisual y Medios Digitales,Comunicación Corporativa,Comunicación y Diseño Gráfico,Comunicación y Periodismo,Comunicación y Publicidad,Comunicación";
      item.texto = "Este ambiente se encuentra equipado con computadoras especiales para edición de audio y video, como también para realizar posproducción, titulación, musicalización, doblaje, entre otros.";
      data.push(item);
      var item = {};
      item.ambiente = "TALLER DE AUDIO";
      item.facultad = 3;
      item.carreras = "Comunicación Audiovisual y Medios Digitales,Comunicación Corporativa,Comunicación y Diseño Gráfico,Comunicación y Periodismo,Comunicación y Publicidad,Comunicación";
      item.foto = "foto_cajamarca_14.jpg";
      item.texto = "En este ambiente contamos con la capacidad para desarrollar proyectos sonoros ya sea solo musicalización, efectos, programas radiales, podcats, entrevista, entre otros.";
      data.push(item);


      var item = {};
      item.ambiente = "LABORATORIO DE FÍSICA";
      item.texto = "Nuestros laboratorios cuentan con equipos digitales, sensores de medición y elementos computarizados que permiten el análisis de los fenómenos físicos de forma práctica y dando una gran experiencia enseñanza-aprendizaje.";
      item.foto = "foto_cajamarca_3.jpg";
      item.facultad = 4;
      item.carreras = "Ingeniería Industrial,Ingeniería Civil,Ingeniería Ambiental,Ingeniería Empresarial,Ingeniería de Minas,Ingeniería Geológica,Ingeniería en Sistemas Computacionales,Ingeniería Agroindustrial";
      item.facultad_label = "INGENIERÍA";
      data.push(item);
      var item = {};
      item.ambiente = "LABORATORIO DE QUÍMICA";
      item.texto = "Este laboratorio cuenta con los equipos de seguridad necesarios para poder trabajar con reactivos químicos, como campanas de gases, duchas y lava ojos de emergencia. Además contamos con los equipos necesarios para el desarrollo de nuestras prácticas experimentales.";
      item.foto = "foto_cajamarca_4.jpg";
      item.facultad = 4;
      item.carreras = "Ingeniería Industrial,Ingeniería Civil,Ingeniería Ambiental,Ingeniería Empresarial,Ingeniería de Minas,Ingeniería Geológica";
      data.push(item);

      var item = {};
      item.ambiente = "LABORATORIO DE MINERÍA Y MEDIO AMBIENTE";
      item.foto = "foto_cajamarca_15.jpg";
      item.facultad = 4;
      item.carreras = "Ingeniería de Minas ,Ingeniería Geológica";

      item.texto = "Este laboratorio cuenta con un equipo de absorción atómica para ensayos de mineral, y se utiliza en cursos como Química Analítica, Concentración de Minerales, Cierre de Minas, entre otros.";
      data.push(item);
      var item = {};
      item.ambiente = "LABORATORIO DE CONCRETO Y ESTRUCTURAS";
      item.foto = "UPN37527.jpg";
      item.facultad = 4;
      item.carreras = "Ingeniería Civil";
      item.texto = "Este laboratorio nos permite estudiar la resistencia del concreto y otros materiales, como ladrillo, madera, acero, etc. para su correcto empleo en obras.";
      data.push(item);
      var item = {};
      item.ambiente = "LABORATORIO DE SUELOS";
      item.foto = "UPN32802.jpg";
      item.facultad = 4;
      item.carreras = "Ingeniería Civil";
      item.texto = "Este laboratorio nos permite estudiar el comportamiento físico y mecánico de los suelos y pavimentos, para conocer su resistencia y aplicación en vías de tránsito y transporte.";
      data.push(item);
      var item = {};
      item.ambiente = "LABORATORIO DE MECÁNICA DE ROCAS";
      item.foto = "rocas.jpg";
      item.facultad = 4;
      item.carreras = "Ingeniería de Minas,Ingeniería Geológica,Ingeniería Civil";
      item.texto = "En este ambiente realizamos la aplicación sobre el comportamiento mecánico de las rocas, investigando y simulando la respuesta de la roca a los campos de fuerza de su entorno físico. Revisamos parámetros relacionados con los diseños de las excavaciones subterráneas como: túneles, galerías, pozos y mapas de tensiones de un yacimiento mineral. Y cuenta con el equipamiento adecuado para tales fines como: perforadora de núcleos para toma de muestras, sierra de diamante, entre otros.";
      data.push(item);
      var item = {};
      item.ambiente = "LABORATORIO DE HIDRÁULICA";
      item.foto = "foto_cajamarca_19.jpg";
      item.facultad = 4;
      item.carreras = "Ingeniería Civil,Ingeniería de Minas,Ingeniería Ambiental";
      item.texto = "Este laboratorio nos permite estudiar el comportamiento y propiedades del agua para trabajar en proyectos de obras hidráulicas.";
      data.push(item);
      var item = {};
      item.ambiente = "LABORATORIO DE AUTOMATIZACIÓN";
      item.foto = "foto_cajamarca_20.jpg";
      item.facultad = 4;
      item.carreras = "Ingeniería Industrial,Ingeniería Electrónica";
      item.texto = "Este ambiente cuenta con equipos de automatización electroneumática y con controladores lógicos donde podemos trabajar programación industrial. Utilizamos software de marcas reconocidas a nivel mundial que permiten complementar la instrucción teórica.";
      data.push(item);
      for(var i=0;i<data.length;i++){ data[i].id = i+1; }
      return (<Campus titulo="Cajamarca" folder="cajamarca" fotoname="CAJ" data={data} nro_mapa={4}></Campus>);
  }
}
export default Cajamarca;
