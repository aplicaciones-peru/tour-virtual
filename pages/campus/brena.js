import Layout from '../../components/layout'
import Pop from '../../components/pop'
import Campus from '../../components/campus'
import styles from './campus.module.css'

class Brena extends React.Component {
constructor(props) {
 super(props);
 this.state = {showInfo:false}
}
 render(){
   var data = new Array();
   var item = {};
   item.ambiente = "FACHADA";
   item.texto = "Somos una universidad licenciada por SUNEDU. Contamos con la acreditación institucional de IAC-CINDA y con programas acreditados por ICACIT y SINEACE. Además, tenemos una alianza exclusiva con CNN y estamos respaldados internacionalmente por QS Stars.";
   item.video = "eSQr8DbQTBs";
   item.foto = "foto_brena_1.jpg";
   item.facultad = 1;
   item.facultad_label = "Áreas Comunes";
  data.push(item);
   var item = {};
   item.ambiente = "PASADIZO PRINCIPAL";
   item.texto = "Accederas a tus clases desde blackboard, la plataforma de educación número 1 en el mundo.";
   //item.video = "2pXewcJmdTA";
   item.foto = "foto_brena_2.jpg";
   item.facultad = 1;
  data.push(item);
   var item = {};
   item.ambiente = "AUDITORIO";
   item.texto = "9 de cada 10 de nuestros egresados, consiguen trabajo en su primer año de egreso.";
   item.video = "2pXewcJmdTA";
   item.foto = "foto_brena_3.jpg";
   item.facultad = 1;
  data.push(item);

   var item = {};
   item.ambiente = "LABORATORIO DE CÓMPUTO";
   item.facultad = 1;
  data.push(item);

   var item = {};
   item.ambiente = "AULA";
   item.texto = "";
   item.foto = "foto_aula.jpg";
   item.facultad = 1;
  data.push(item);

   var item = {};
   item.ambiente = "TERRAZA";
   item.texto = "Podrás acceder a Empléate UPN, un programa integral donde recibes asesorías y entrenamiento virtual / Te ofrecemos Contacto UPN, una herramienta de respuesta ágil para resolver todas tus consultas.";
   item.video = "xjeeaF9ZHKU";
   item.foto = "foto_brena_4.jpg";
   item.facultad = 1;
  data.push(item);
   var item = {};
   item.ambiente = "LOSA DEPORTIVA";
   item.texto = "Desarrollamos para ti un sistema de acompañamiento remoto con programas de nivelación, orientación personal, talleres xtracurriculares y charlas.";
   item.facultad = 1;
  data.push(item);

   var item = {};
   item.ambiente = "CAFETERÍA";
   item.texto = "Somos mas de 80 mil estudiantes, interactuando con el mejor ecosistema multiplataforma de educación virtual.";
   item.foto = "foto_brena_6.jpg";
   item.video = "vdiLD5e6A9c";
   item.facultad = 1;
  data.push(item);
   var item = {};

   var item = {};
   item.ambiente = "TALLER DE FOTOGRAFÍA";
   item.texto = " Este taller cuenta con equipos de fotografía e iluminación para realizar diversos proyectos como retratos, fotos de productos, alimentos, entre otros.";
   item.foto = "foto_brena_8.jpg";
   item.facultad = 3;
   item.facultad_label = "COMUNICACIONES";
   item.carreras = "Comunicación Audiovisual y Medios Digitales,Comunicación Corporativa,Comunicación y Diseño Gráfico,Comunicación y Periodismo,Comunicación y Publicidad,Comunicación";

  data.push(item);
   var item = {};
   item.ambiente = "LAB ZAM";
   item.texto = "En este ambiente contamos con un set de televisión y equipamiento necesario para preparar un producto audiovisual grabado o en vivo. Ademas contamos con escenografía digital para poder realizar cualquier tipo de ambientación.";
   item.foto = "foto_brena_9.jpg";
   item.facultad = 3;
   item.carreras = "Comunicación Audiovisual y Medios Digitales,Comunicación Corporativa,Comunicación y Diseño Gráfico,Comunicación y Periodismo,Comunicación y Publicidad,Comunicación";
  data.push(item);

   var item = {};
   item.ambiente = "TALLER DE EDICIÓN";
   item.texto = "Este ambiente se encuentra equipado con computadoras especiales para edición de audio y video, como también para realizar posproducción, titulación, musicalización, doblaje, entre otros.";
   item.facultad = 3;
   item.carreras = "Comunicación Audiovisual y Medios Digitales,Comunicación Corporativa,Comunicación y Diseño Gráfico,Comunicación y Periodismo,Comunicación y Publicidad,Comunicación";
  data.push(item);
   var item = {};
   item.ambiente = "TALLER DE AUDIO";
   item.texto = " En este ambiente contamos con la capacidad para desarrollar proyectos sonoros ya sea solo musicalización, efectos, programas radiales, podcats, entrevista, entre otros.";
   item.foto = "foto_brena_11.jpg";
   item.carreras = "Comunicación Audiovisual y Medios Digitales,Comunicación Corporativa,Comunicación y Diseño Gráfico,Comunicación y Periodismo,Comunicación y Publicidad,Comunicación";
   item.facultad = 3;
  data.push(item);
   var item = {};
   item.ambiente = "LABORATORIO DE ELECTRÓNICA";
   item.texto = "Este ambiente cuenta con equipos de medición electrónica para realizar prácticas de circuitos, como Osciloscopios, Generadores de ondas, Multimetros, y componentes electrónicos diversos.";
   item.foto = "foto_brena_12.jpg";
   item.facultad = 4;
   item.facultad_label = "INGENIERÍA";
   item.carreras = "Ingeniería Electrónica,Ingeniería Mecatrónica,Ingeniería de Sistemas Computacionales ";
  data.push(item);
   var item = {};
   item.texto = "Nuestros laboratorios cuentan con equipos digitales, sensores de medición y elementos computarizados que permiten el análisis de los fenómenos físicos de forma práctica y dando una gran experiencia enseñanza-aprendizaje.";
   item.ambiente = "LABORATORIO DE FÍSICA";
   item.carreras = "Ingeniería Industrial,Ingeniería Civil,Ingeniería Ambiental,Ingeniería Empresarial,Ingeniería de Minas,Ingeniería Geológica,Ingeniería en Sistemas Computacionales,Ingeniería Agroindustrial";
   item.facultad = 4;
   item.foto = "foto_brena_13.jpg";
  data.push(item);
   var item = {};
   item.ambiente = "LABORATORIO DE QUÍMICA";
   item.texto = "Este laboratorio cuenta con los equipos de seguridad necesarios para poder trabajar con reactivos químicos, como campanas de gases, duchas y lava ojos de emergencia. Además contamos con los equipos necesarios para el desarrollo de nuestras prácticas experimentales.";
   item.foto = "foto_brena_14.jpg";
   item.carreras = "Ingeniería Industrial,Ingeniería Civil,Ingeniería Ambiental,Ingeniería Empresarial,Ingeniería de Minas,Ingeniería Geológica";
   item.facultad = 4;
  data.push(item);
   var item = {};
   item.ambiente = "LABORATORIO DE CONCRETO";
   item.texto = "Este laboratorio nos permite estudiar la resistencia del concreto y otros materiales, como ladrillo, madera, acero, etc. para su correcto empleo en obras.";
   item.foto = "foto_brena_15.jpg";
   item.carreras = " Ingeniería Civil";
   item.facultad = 4;
  data.push(item);
   var item = {};
   item.ambiente = "LABORATORIO HIDRÁULICA";
   item.texto = "Este laboratorio nos permite estudiar el comportamiento y propiedades del agua para trabajar en proyectos de obras hidráulicas.";
   item.foto = "foto_brena_16.jpg";
   item.carreras = "Ingeniería Civil,Ingeniería de Minas,Ingeniería Ambiental";
   item.facultad = 4;
  data.push(item);

   var item = {};
   item.ambiente = "GABINETE DE TOPOGRAFÍA MINERA";
   item.texto = "Contamos con un gabinete donde guardamos los equipos topográficos para realizar practicas en la universidad o en campo.";
   item.foto = "foto_topografia.jpg";
   item.carreras = "Ingeniería de Minas";
   item.facultad = 4;
  data.push(item);
   var item = {};
   item.ambiente = "LABORATORIO SUELOS";
   item.carreras = " Ingeniería Civil";
   item.texto = "Este laboratorio nos permite estudiar el comportamiento físico y mecánico de los suelos y pavimentos, para conocer  su resistencia y aplicación en vías de tránsito y transporte.";
   item.foto = "foto_brena_17.jpg";
   item.facultad = 4;
  data.push(item);
   var item = {};
   item.ambiente = "TALLER DE COCINA";
   item.carreras = "Gastronomía y Gestión de Restaurantes,Administración y Servicios Turísticos";
   item.texto = "Aquí contamos con las herramientas y equipos necesarios para desarrollar técnicas culinarias, cocina nacional e internacional, panadería, pastelería, entre otros.";
   item.foto = "foto_brena_18.jpg";
   item.facultad_label = "NEGOCIOS";
   item.facultad = 5;
  data.push(item);
   var item = {};
   item.ambiente = "TALLER DE HOUSEKEEPING";
   item.carreras = "Administración y Servicios Turísticos";
   item.texto = "Este taller recrea la habitación de un hotel con todos los implementos necesarios, y en ella podremos practicar distintas actividades del área de Housekeeping.";
   item.facultad = 5;
  data.push(item);
   for(var i=0;i.length;i++){ data[i].id = i+1; }
   return (<Campus titulo="Breña" folder="brena" fotoname="BRENA" data={data} nro_mapa={3}></Campus>);
}
}
export default Brena;
