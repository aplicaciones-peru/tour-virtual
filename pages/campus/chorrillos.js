import Layout from '../../components/layout'
import Pop from '../../components/pop'
import Campus from '../../components/campus'
import styles from './campus.module.css'

class Chorrillos extends React.Component {
  constructor(props) {
    super(props);
    this.state = {showInfo:false}
  }
  render(){
      var data = new Array();

      var item = {};
      item.ambiente = "FACHADA";
      item.texto = "Somos una universidad licenciada por SUNEDU. Contamos con la acreditación institucional de IAC-CINDA y con programas acreditados por ICACIT y SINEACE. Además, tenemos una alianza exclusiva con CNN y estamos respaldados internacionalmente por QS Stars.";
      item.video = "eSQr8DbQTBs";
      item.foto = "foto_chorrillos_1.jpg";
      item.facultad = 1;
      item.facultad_label = "Áreas Comunes";
      data.push(item);
      var item = {};
      item.ambiente = "PLAZA";
      item.texto = "Accederás a tus clases desde blackboard, la plataforma de educación número 1 en el mundo.";
      item.video = "xjeeaF9ZHKU";
      item.foto = "foto_chorrillos_2.jpg";
      item.facultad = 1;
      data.push(item);
      var item = {};
      item.ambiente = "CENTRO DE INFORMACIÓN";
      item.texto = "Tendrás a tu alcance mas de 380 mil materiales de lectura para complementar tu educación fisica y virtual.";
      item.video = "Upbk6x9ZQtk";
      item.foto = "foto_chorrillos_3.jpg";
      item.facultad = 1;
      data.push(item);
      var item = {};
      item.ambiente = "LABORATORIO DE CÓMPUTO";
      data.push(item);
      var item = {};
      item.ambiente = "AULA";
      item.foto = "foto_chorrillos_5.jpg";
      item.facultad = 1;
      item.texto = "";
      data.push(item);
      var item = {};
      item.ambiente = "CAFETERÍA";
      item.texto = "Somos mas de 80 mil estudiantes, interactuando con el mejor ecosistema multiplataforma de educación virtual.";
      item.video = "vdiLD5e6A9c";
      item.foto = "foto_chorrillos_6.jpg";
      item.facultad = 1;
      data.push(item);


      var item = {};
      item.ambiente = "LABORATORIO DE ESTRUCTURA Y FUNCIÓN";
      item.texto = "En este laboratorio aprendemos sobre los órganos que conforman el cuerpo humano y el funcionamiento de los mismos mediante maquetas, aplicaciones en tablets, pintura corporal, proyección de imágenes y videos y anatomía palpatoria entre los mismos estudiantes.";
      item.foto = "foto_chorrillos_7.jpg";
      item.facultad = 2;
      item.carreras = "Enfermería,Obstetricia,Nutrición y Dietética,Terapia Física y Rehabilitación,Psicología";
      item.facultad_label = "SALUD";
      data.push(item);

      var item = {};
      item.ambiente = "TALLER DE EDICIÓN";
      item.texto = "Este ambiente se encuentra equipado con computadoras especiales para edición de audio y video, como también para realizar posproducción, titulación, musicalización, doblaje, entre otros.";
      item.facultad = 3;
      item.carreras = "Comunicación Audiovisual y Medios Digitales,Comunicación Corporativa,Comunicación y Diseño Gráfico,Comunicación y Periodismo,Comunicación y Publicidad,Comunicación";
      item.facultad_label = "COMUNICACIONES";
      data.push(item);

      var item = {};
      item.ambiente = "TALLER DE FOCUS GROUP";
      item.texto = "Este ambiente se encuentra completamente equipado con sistemas de audio y video y una pc que podra grabar nuestras sesiones de focus group.";
      item.facultad = 4;
      item.carreras = "Administración y Marketing";
      item.facultad_label = "NEGOCIOS";
      data.push(item);
      for(var i=0;i<data.length;i++){ data[i].id = i+1; }
      return (<Campus titulo="Chorrillos" folder="chorrillos" fotoname="CHORRILLOS" titulo="Chorrillos" data={data} nro_mapa={2}></Campus>);
  }
}
export default Chorrillos;
