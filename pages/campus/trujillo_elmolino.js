import Layout from '../../components/layout'
import Pop from '../../components/pop'
import Campus from '../../components/campus'
import styles from './campus.module.css'

class Trujillo_elmolino extends React.Component {
  constructor(props) {
    super(props);
    this.state = {showInfo:false}
  }
  render(){
    var data = new Array();
    var item = {};
    item.ambiente = "FACHADA";
    item.texto = "Somos una universidad licenciada por SUNEDU. Contamos con la acreditación institucional de IAC-CINDA y con programas acreditados por ICACIT y SINEACE. Además, tenemos una alianza exclusiva con CNN y estamos respaldados internacionalmente por QS Stars.";
    item.foto = "foto_trujillo_elmolino_1.jpg";
    item.video = "eSQr8DbQTBs";
    item.facultad = 1;
    item.facultad_label = "Áreas Comunes";
    data.push(item);
    var item = {};
    item.ambiente = "PLAZA";
    item.texto = "Accederás a tus clases desde blackboard, la plataforma de educación número 1 en el mundo.";
    item.foto = "foto_trujillo_elmolino_2.jpg";
    item.facultad = 1;
    data.push(item);
    var item = {};
    item.ambiente = "CAFETERÍA";
    item.texto = "Somos mas de 80 mil estudiantes, interactuando con el mejor ecosistema multiplataforma de educación virtual.";
    item.foto = "foto_trujillo_elmolino_3.jpg";
    item.video = "vdiLD5e6A9c";
    item.facultad = 1;
    data.push(item);
    var item = {};
    item.ambiente = "CENTRO DE INFORMACIÓN 1";
    item.texto = "Tendrás a tu alcance mas de 380 mil materiales de lectura para complementar tu educación física y virtual.";
    item.foto = "foto_trujillo_elmolino_4.jpg";
    item.video = "Upbk6x9ZQtk";
    item.facultad = 1;
    data.push(item);

    var item = {};
    item.ambiente = "CENTRO DE INFORMACIÓN 2";
    item.texto = "Tendrás a tu alcance mas de 380 mil materiales de lectura para complementar tu educación física y virtual.";
    item.foto = "foto_trujillo_elmolino_4.jpg";
    item.video = "Upbk6x9ZQtk";
    item.facultad = 1;
    data.push(item);

    var item = {};
    item.ambiente = "LABORATORIO DE CÓMPUTO";
    //item.texto = "9 de cada 10 de nuestros egresados, consiguen trabajo en su primer año de egreso";
    //item.video = "xjeeaF9ZHKU";
    item.facultad = 1;
    data.push(item);

    var item = {};
    item.ambiente = "AULA";
    item.texto = "";
    //item.video = "xjeeaF9ZHKU";
    item.foto = "foto_aula.jpg";
    item.facultad = 1;
    data.push(item);

/*
    var item = {};
    item.ambiente = "LABORATORIO MULTIFUNCIONAL";
    item.texto = "Este es un espacio multifuncional, equipado con simuladores para el entrenamiento de habilidades y destrezas psicomotoras en diferentes procedimientos médicos, como colocación de inyectables, reanimación cardio-respiratoria, inserción de sondas y catéteres, entre otros.";
    item.foto = "foto_trujillo_elmolino_5.jpg";
    item.carreras = "Enfermería,Obstetricia,Nutrición y Dietética,Terapia Física y Rehabilitación";
    item.facultad = 2;
    item.facultad_label = "SALUD";
    data.push(item);*/

    var item = {};
    item.ambiente = "TALLER DE FOTOGRAFÍA";
    item.texto = "Este taller cuenta con equipos de fotografía e iluminación para realizar diversos proyectos como retratos, fotos de productos, alimentos, entre otros.";
    item.foto = "foto_trujillo_elmolino_6.jpg";
    item.facultad = 3;
    item.facultad_label = "COMUNICACIONES";
    item.carreras = "Comunicación Audiovisual y Medios Digitales,Comunicación Corporativa,Comunicación y Diseño Gráfico,Comunicación y Periodismo,Comunicación y Publicidad,Comunicación";
    data.push(item);
    var item = {};
    item.ambiente = "TALLER DE EDICIÓN";
    item.texto = "Este ambiente se encuentra equipado con computadoras especiales para edición de audio y video, como tambien para realizar posproducción, titulación, musicalización, doblaje, entre otros.";
    //item.foto = "foto_trujillo_elmolino_7.jpg";
    item.carreras = "Comunicación Audiovisual y Medios Digitales,Comunicación Corporativa,Comunicación y Diseño Gráfico,Comunicación y Periodismo,Comunicación y Publicidad,Comunicación";
    item.facultad = 3;
    data.push(item);
    var item = {};
    item.ambiente = "TALLER DE AUDIO";
    item.texto = " En este ambiente contamos con la capacidad para desarrollar proyectos sonoros ya sea solo musicalización, efectos, programas radiales, podcats, entrevista, entre otros.";
    item.foto = "foto_trujillo_elmolino_8.jpg";
    item.carreras = "Comunicación Audiovisual y Medios Digitales,Comunicación Corporativa,Comunicación y Diseño Gráfico,Comunicación y Periodismo,Comunicación y Publicidad,Comunicación";
    item.facultad = 3;
    data.push(item);
    var item = {};
    item.ambiente = "LAB ZAM";
    item.texto = "En este ambiente contamos con un set de televisión y equipamiento necesario para preparar un producto audiovisual grabado o en vivo. Además contamos con escenografía digital para poder realizar cualquier tipo de ambientación.";
    item.foto = "foto_trujillo_elmolino_9.jpg";
    item.carreras = "Comunicación Audiovisual y Medios Digitales,Comunicación Corporativa,Comunicación y Diseño Gráfico,Comunicación y Periodismo,Comunicación y Publicidad,Comunicación";
    item.facultad = 3;
    data.push(item);
/*
    var item = {};
    item.ambiente = "SWITCHER";
    item.texto = "En este ambiente contamos con un set de televisión y equipamiento necesario para preparar un producto audiovisual grabado o en vivo. Además contamos con escenografía digital para poder realizar cualquier tipo de ambientación.";
    item.foto = "foto_trujillo_elmolino_9.jpg";
    item.carreras = "Comunicación Audiovisual y Medios Digitales,Comunicación Corporativa,Comunicación y Diseño Gráfico,Comunicación y Periodismo,Comunicación y Publicidad,Comunicación";
    item.facultad = 3;
    data.push(item);
*/
    var item = {};
    item.ambiente = "LABORATORIO DE ESTRUCTURA Y FUNCIÓN";
    item.facultad = 4;
    item.facultad_label = "Salud";
    item.carreras = "Enfermería,Obstetricia,Nutrición y Dietética,Terapia Física y Rehabilitación,Psicología";
    item.foto = "foto_laboratorio.jpg";
    item.texto = "En este laboratorio aprendemos sobre los órganos que conforman el cuerpo humano y el funcionamiento de los mismos mediante maquetas, aplicaciones en tablets, pintura corporal, proyección de imágenes y videos y anatomía palpatoria entre los mismos estudiantes.";
    data.push(item);


    for(var i=0;i<data.length;i++){ data[i].id = i+1; }
      return (<Campus titulo="Trujillo - El Molino" folder="trujillo_elmolino" fotoname="TRU_ELMOLINO" data={data} nro_mapa={5}></Campus>);
  }
}
export default Trujillo_elmolino;
