import Layout from '../../components/layout'
import Pop from '../../components/pop'
import Campus from '../../components/campus'
import styles from './campus.module.css'

class Comas extends React.Component {
  constructor(props) {
    super(props);
    this.state = {showInfo:false}
  }
  render(){
      var data = new Array();
      var item = {};
      item.ambiente = "FACHADA";
      item.texto = "Somos una universidad licenciada por SUNEDU. Contamos con la acreditación institucional de IAC-CINDA y con programas acreditados por ICACIT y SINEACE. Además, tenemos una alianza exclusiva con CNN y estamos respaldados internacionalmente por QS Stars.";
      item.video = "eSQr8DbQTBs";
      item.foto = "foto_comas_1.jpg";
      item.facultad = 1;
      item.facultad_label = "Áreas Comunes";
      data.push(item);
      var item = {};
      item.ambiente = "PASADIZO PRINCIPAL";
      item.texto = "Accederás a tus clases desde blackboard, la plataforma de educación número 1 en el mundo.";
      item.foto = "foto_comas_2.jpg";
      item.facultad = 1;
      data.push(item);
      var item = {};
      item.ambiente = "TERRAZA";
      item.texto = "Podrás acceder a Empleate UPN, un programa integral donde recibes asesorías y entrenamiento virtual / Te ofrecemos Contacto UPN, una herramienta de respuesta ágil para resolver todas tus consultas.";
      item.video = "xjeeaF9ZHKU";
      item.foto = "foto_comas_3.jpg";
      item.facultad = 1;
      data.push(item);
      var item = {};
      item.ambiente = "LOSA DEPORTIVA";
      item.texto = "Desarrollamos para ti un sistema de acompañamiento remoto con programas de nivelación, orientación personal, talleres extracurriculares y charlas.";
      item.facultad = 1;
      data.push(item);
      var item = {};
      item.ambiente = "CAFETERÍA";
      item.video = "vdiLD5e6A9c";
      item.foto = "foto_comas_5.jpg";
      item.facultad = 1;
      item.texto = "Somos mas de 80 mil estudiantes, interactuando con el mejor ecosistema multiplataforma de educación virtual.";
      data.push(item);
      var item = {};
      item.ambiente = "CENTRO DE INFORMACIÓN";
      item.texto = "Tendras a tu alcance mas de 380 mil materiales de lectura para complementar tu educación fisica y virtual.";
      item.video = "Upbk6x9ZQtk";
      item.foto = "foto_comas_6.jpg";
      item.facultad = 1;
      data.push(item);


      var item = {};
      item.ambiente = "LABORATORIO DE CÓMPUTO";
      item.facultad = 1;
      data.push(item);

      var item = {};
      item.ambiente = "AULA";
      item.facultad = 1;
      item.texto = "";
      item.foto = "foto_aula.jpg";
      data.push(item);


      /*var item = {};
      item.ambiente = "LABORATORIO MULTIFUNCIONAL";
      item.texto = "";
      item.foto = "foto_comas_7.jpg";
      data.push(item);*/
      var item = {};
      item.ambiente = "TALLER DE DISEÑO INDUSTRIAL";
      item.texto = "Aquí desarrollamos prototipos y nuevas creaciones, tales como objetos de uso comun o diseños complejos gracias a la maquinaria y equipos modernos con los que contamos.";
      item.foto = "foto_comas_8.jpg";
      item.facultad = 2;
      item.facultad_label = "ARQUITECTURA Y DISEÑO";
      item.carreras = "Diseño Industrial";
      data.push(item);
      var item = {};
      item.ambiente = "TALLER DE SOLDADURA";
      item.texto = "En este espacio trabajamos los materiales de acero y forjados para desarrollar nuevas creaciones. También contamos con ambientes para realizar el pintado de nuestros prototipos.";
      item.foto = "foto_comas_9.jpg";
      item.facultad = 2;
      item.carreras = "Diseño Industrial";
      data.push(item);

      var item = {};
      item.ambiente = "TALLER DE CREATIVIDAD";
      item.texto = "Aquí contamos con mesas de trabajo para desarrollar proyectos en equipo, además de una pizarra interactiva que favorece el trabajo de manera dinámica para los cursos de emprendimiento e innovación.";
      item.facultad = 2;
      item.foto = "creatividad.jpg";
      item.carreras = "Arquitectura y Diseño de Interiores ,Arquitectura y Urbanismo,Diseño Industrial";
      data.push(item);


      var item = {};
      item.ambiente = "TALLER DE AUDIO";
      item.texto = " En este ambiente contamos con la capacidad para desarrollar proyectos sonoros ya sea solo musicalización, efectos, programas radiales, podcats, entrevista, entre otros.";
      item.foto = "foto_comas_10.jpg";
      item.carreras = "Comunicación Audiovisual y Medios Digitales,Comunicación Corporativa,Comunicación y Diseño Gráfico,Comunicación y Periodismo,Comunicación y Publicidad,Comunicación";
      item.facultad_label = "COMUNICACIONES";
      item.facultad = 3;
      data.push(item);
      var item = {};
      item.ambiente = "TALLER DE FOTOGRAFÍA";
      item.texto = " Este taller cuenta con equipos de fotografía e iluminación para realizar diversos proyectos como retratos, fotos de productos, alimentos, entre otros.";
      item.foto = "foto_comas_11.jpg";
      item.facultad = 3;

      item.carreras = "Comunicación Audiovisual y Medios Digitales,Comunicación Corporativa,Comunicación y Diseño Gráfico,Comunicación y Periodismo,Comunicación y Publicidad,Comunicación";
      data.push(item);
      var item = {};
      item.ambiente = "LAB ZAM";
      item.texto = "En este ambiente contamos con un set de televisión y equipamiento necesario para preparar un producto audiovisual grabado o en vivo. Además contamos con escenografía digital para poder realizar cualquier tipo de ambientación.";
      item.foto = "foto_comas_12.jpg";
      item.carreras = "Comunicación Audiovisual y Medios Digitales,Comunicación Corporativa,Comunicación y Diseño Gráfico,Comunicación y Periodismo,Comunicación y Publicidad,Comunicación";
      item.facultad = 3;
      data.push(item);
      var item = {};
      item.ambiente = "LABORATORIO DE DISEÑO";
      item.texto = "En este laboratorio contamos con equipos Macintosh y software especializado para realizar trabajos de diseño gráfico y multimedia.";
      item.foto = "foto_comas_13.jpg";
      item.carreras = "Comunicación y Diseño Gráfico";
      item.facultad = 3;
      data.push(item);

      for(var i=0;i<data.length;i++){ data[i].id = i+1; }
      return (<Campus titulo="Comas" folder="comas" fotoname="COMAS" data={data} nro_mapa={8}></Campus>);
  }
}
export default Comas;
