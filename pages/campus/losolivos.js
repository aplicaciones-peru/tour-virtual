import Layout from '../../components/layout'
import Pop from '../../components/pop'
import Campus from '../../components/campus'
import styles from './campus.module.css'

class Losolivos extends React.Component {
  constructor(props) {
    super(props);
    this.state = {showInfo:false}
  }
  render(){
      var data = new Array();
      var item = {};
      item.ambiente = "FACHADA";
      item.texto = "Somos una universidad licenciada por SUNEDU. Contamos con la acreditación institucional de IAC-CINDA y con programas acreditados por ICACIT y SINEACE. Además, tenemos una alianza exclusiva con CNN y estamos respaldados internacionalmente por QS Stars.";
      item.video = "eSQr8DbQTBs";
      item.foto = "foto_losolivos_1.jpg";
      item.facultad = 1;
      item.facultad_label = "Áreas Comunes";
      data.push(item);
      var item = {};
      item.ambiente = "PASADIZO PRINCIPAL";
      item.texto = "Accederás a tus clases desde blackboard, la plataforma de educación número 1 en el mundo.";
      item.foto = "foto_losolivos_2.jpg";
      item.facultad = 1;
      data.push(item);
      var item = {};
      item.ambiente = "PLAZA";
      item.texto = "Te ofrecemos Contacto UPN, una herramienta de respuesta agil para resolver todas tus consultas.";
      item.foto = "foto_losolivos_3.jpg";
      item.facultad = 1;
      data.push(item);
      var item = {};
      item.ambiente = "AUDITORIO";
      item.video = "2pXewcJmdTA";
      item.foto = "foto_losolivos_5.jpg";
      item.facultad = 1;
      item.texto = "9 de cada 10 de nuestros egresados, consiguen trabajo en su primer año de egreso.";
      data.push(item);
      var item = {};
      item.ambiente = "TERRAZA";
      item.texto = "Podrás acceder a Empleate UPN, un programa integral donde recibes asesorías y entrenamiento virtual / Te ofrecemos Contacto UPN, una herramienta de respuesta ágil para resolver todas tus consultas.";
      item.foto = "foto_losolivos_6.jpg";
      item.facultad = 1;
      item.video = "xjeeaF9ZHKU";
      data.push(item);
      var item = {};
      item.ambiente = "CAFETERÍA";
      item.texto = "Somos mas de 80 mil estudiantes, interactuando con el mejor ecosistema multiplataforma de educación virtual.";
      item.foto = "foto_losolivos_7.jpg";
      item.facultad = 1;
      item.video = "vdiLD5e6A9c";
      data.push(item);
      var item = {};
      item.ambiente = "CENTRO DE INFORMACIÓN";
      item.facultad = 1;
      item.texto = "Tendrás a tu alcance mas de 380 mil materiales de lectura para complementar tu educación fisica y virtual.";
      item.foto = "foto_losolivos_8.jpg";
      item.video = "Upbk6x9ZQtk";
      data.push(item);

      var item = {};
      item.ambiente = "LABORATORIO DE CÓMPUTO";
      item.facultad = 1;
      data.push(item);

      var item = {};
      item.ambiente = "AULA";
      item.foto = "foto_aula.jpg";
      item.facultad = 1;
      item.texto = "";
      data.push(item);


      var item = {};
      item.ambiente = "LABORATORIO DE ALTA COMPLEJIDAD";
      item.facultad = 2;
      item.facultad_label = "SALUD";
      item.texto = "Este es un espacio simulado de una sala de emergencias, unidad de shock trauma, sala de operaciones, unidad de cuidados intensivos y/o sala de partos para nuestro entrenamiento en atención de pacientes graves usando equipos de alta complejidad.";
      item.foto = "foto_losolivos_9.jpg";
      item.carreras = "Enfermería,Obstetricia,Nutrición y Dietética,Terapia Física y Rehabilitación";
      data.push(item);
      var item = {};
      item.ambiente = "LABORATORIO DE HOSPITAL SIMULADO";
      item.facultad = 2;
      item.texto = "En este espacio recreamos los ambientes de internamiento de un hospital general para el entrenamiento de atención a los pacientes, usando simuladores que son capaces de reaccionar a estimulos como si fueran pacientes reales.";
      item.foto = "foto_losolivos_10.jpg";
      item.carreras = "Enfermería,Obstetricia,Nutrición y Dietética,Terapia Física y Rehabilitación,Psicología";
      data.push(item);
      var item = {};
      item.ambiente = "LABORATORIO DE ESTRUCTURA Y FUNCIÓN";
      item.facultad = 2;
      item.texto = "En este laboratorio aprendemos sobre los organos que conforman el cuerpo humano y el funcionamiento de los mismos mediante maquetas, aplicaciones en tablets, pintura corporal, proyección de imágenes y videos y anatomía palpatoria entre los mismos estudiantes.";
      item.foto = "foto_losolivos_11.jpg";
      item.carreras = "Enfermería,Obstetricia,Nutrición y Dietética,Terapia Física y Rehabilitación,Psicología";
      data.push(item);
      var item = {};
      item.ambiente = "LABORATORIO DE TERAPIA FÍSICA";
      item.facultad = 2;
      item.texto = "Recreamos un ambiente real de terapia física y rehabilitación para el entrenamiento de las habilidades y destrezas en la atención de pacientes con limitaciones del movimiento.";
      item.foto = "foto_losolivos_12.jpg";
      item.carreras = "Terapia Física y Rehabilitación";
      data.push(item);



      var item = {};
      item.ambiente = "LABORATORIO DE MECATRÓNICA";
      item.facultad = 3;
      item.facultad_label = "INGENIERÍA";
      item.texto = "Este ambiente cuenta con implementos de última generación, como equipos de Robótica industrial, Control de procesos, Redes industriales e  Impresoras 3D, que nos permiten aprender a programar y realizar aplicaciones industriales.";
      item.foto = "foto_losolivos_13.jpg";
      item.carreras = "Ingeniería Mecatrónica,Ingeniería Electrónica";
      data.push(item);

      var item = {};
      item.ambiente = "LABORATORIO DE QUÍMICA";
      item.facultad = 3;
      item.texto = "Este laboratorio cuenta con los equipos de seguridad necesarios para poder trabajar con reactivos químicos, como campanas de gases, duchas y lava ojos de emergencia. Además contamos con los equipos necesarios para el desarrollo de nuestras prácticas experimentales.";
      item.foto = "foto_losolivos_14.jpg";
      item.carreras = "Ingeniería Industrial,Ingeniería Civil,Ingeniería Ambiental,Ingeniería Empresarial,Ingeniería de Minas,Ingeniería Geológica";
      data.push(item);

      var item = {};
      item.ambiente = "LABORATORIO DE FÍSICA";
      item.facultad = 3;
      item.texto = "Nuestros laboratorios cuentan con equipos digitales, sensores de medición y elementos computarizados que permiten el análisis de los fenómenos físicos de forma práctica y dando una gran experiencia enseñanza-aprendizaje.";
      item.foto = "UPN31752.jpg";
      item.carreras = "Ingeniería Industrial,Ingeniería Civil,Ingeniería Ambiental,Ingeniería Empresarial,Ingeniería de Minas,Ingeniería Geológica,Ingeniería en Sistemas Computacionales,Ingeniería Agroindustrial";
      data.push(item);




      var item = {};
      item.ambiente = "LABORATORIO DE AUTOMATIZACIÓN";
      item.facultad = 3;
      item.texto = "Este ambiente cuenta con equipos de automatización electroneumática y con controladores lógicos donde podemos trabajar programación industrial. Utilizamos softwares de marcas reconocida a nivel mundial que permiten complementar la instrucción teórica.";
      item.foto = "foto_losolivos_15.jpg";
      item.carreras = "Ingeniería Industrial,Ingeniería Civil,Ingeniería Ambiental,Ingeniería Empresarial,Ingeniería de Minas,Ingeniería Geológica,Ingeniería en Sistemas Computacionales,Ingeniería Agroindustrial";
      data.push(item);
      var item = {};
      item.ambiente = "LABORATORIO DE ELECTRÓNICA";
      item.facultad = 3;
      item.texto = "Este ambiente cuenta con equipos de medición electrónica  para realizar prácticas de circuitos, como Osciloscopios, Generadores de ondas, Multimetros, y componentes electrónicos diversos.";
      item.foto = "foto_losolivos_16.jpg";
      item.carreras = "Ingeniería Industrial,Ingeniería Electrónica";
      data.push(item);
      var item = {};
      item.ambiente = "LABORATORIO DE CONCRETO";
      item.facultad = 3;
      item.texto = "Este laboratorio nos permite estudiar la resistencia del concreto y otros materiales, como ladrillo, madera, acero, etc. para su correcto empleo en obras.";
      item.foto = "foto_losolivos_17.jpg";
      item.carreras = "Ingeniería Electrónica ,Ingeniería Mecatrónica ,Ingeniería de Sistemas Computacionales";
      data.push(item);
      var item = {};
      item.ambiente = "LABORATORIO DE HIDRÁULICA";
      item.facultad = 3;
      item.carreras = "Ingeniería Civil";
      item.texto = "Este laboratorio nos permite estudiar el comportamiento y propiedades del agua para trabajar en proyectos de obras hidráulicas.";
      item.foto = "foto_losolivos_18.jpg";
      data.push(item);
      var item = {};
      item.ambiente = "TALLER DE COCINA";
      item.facultad = 4;
      item.facultad_label = "NEGOCIOS";
      item.carreras = "Gastronomía y Gestión de Restaurantes, Administración y Servicios Turísticos";
      item.texto = "Aquí contamos con las herramientas y equipos necesarios para desarrollar técnicas culinarias, cocina nacional e internacional, panadería, pastelería, entre otros.";
      item.foto = "foto_losolivos_19.jpg";
      data.push(item);

      var item = {};
      item.ambiente = "TALLER DE HOUSEKEEPING";
      item.facultad = 4;
      item.carreras = "Administración y Servicios turísticos";
      item.texto = "Este taller recrea la habitación de un hotel con todos los implementos necesarios, y en ella podremos practicar distintas actividades del área de Housekeeping.";
      data.push(item);
      for(var i=0;i<data.length;i++){ data[i].id = i+1; }
      return (<Campus titulo="Los Olivos" folder="losolivos" fotoname="LOSOLIVOS" data={data} nro_mapa={1}></Campus>);
  }
}
export default Losolivos;
