import Layout from '../../components/layout'
import Pop from '../../components/pop'
import Campus from '../../components/campus'
import styles from './campus.module.css'

class Trujillo_sanisidro extends React.Component {
  constructor(props) {
    super(props);
    this.state = {showInfo:false}
  }
  render(){
    var data = new Array();
    var item = {};
    item.ambiente = "FACHADA";
    item.texto = "Somos una universidad licenciada por SUNEDU. Contamos con la acreditación institucional de IAC-CINDA y con programas acreditados por ICACIT y SINEACE. Además, tenemos una alianza exclusiva con CNN y estamos respaldados internacionalmente por QS Stars.";
    item.video = "eSQr8DbQTBs";
    item.facultad = 1;
    item.facultad_label = "Áreas Comunes";
    item.foto = "foto_trujillo_sanisidro_1.jpg";
    data.push(item);
    var item = {};
    item.ambiente = "PASADIZO PRINCIPAL";
    item.texto = "Accederás a tus clases desde blackboard, la plataforma de educación número 1 en el mundo";
    item.foto = "foto_trujillo_sanisidro_2.jpg";
    item.facultad = 1;
    data.push(item);
    var item = {};
    item.ambiente = "PLAZA HUNDIDA";
    item.texto = "Te ofrecemos Contacto UPN, una herramienta de respuesta ágil para resolver todas tus consultas ";
    item.foto = "foto_trujillo_sanisidro_3.jpg";
    item.facultad = 1;
    data.push(item);
    var item = {};
    item.ambiente = "AUDITORIO";
    item.texto = "9 de cada 10 de nuestros egresados, consiguen trabajo en su primer año de egreso";
    item.foto = "foto_trujillo_sanisidro_4.jpg";
    item.video = "2pXewcJmdTA";
    item.facultad = 1;
    data.push(item);
    var item = {};
    item.ambiente = "TERRAZA";
    item.texto = "Podrás acceder a Empleate UPN, un programa integral donde recibes asesorías y entrenamiento virtual / Te ofrecemos Contacto UPN, una herramienta de respuesta ágil para resolver todas tus consultas";
    item.foto = "foto_trujillo_sanisidro_5.jpg";
    item.video = "xjeeaF9ZHKU";
    item.facultad = 1;
    data.push(item);
    var item = {};
    item.ambiente = "CAFETERÍA";
    item.texto = "Somos mas de 80 mil estudiantes, interactuando con el mejor ecosistema multiplataforma de educación virtual";
    item.foto = "foto_trujillo_sanisidro_6.jpg";
    item.video = "vdiLD5e6A9c";
    item.facultad = 1;
    data.push(item);
    var item = {};
    item.ambiente = "CENTRO DE INFORMACIÓN";
    item.texto = "Tendrás a tu alcance mas de 380 mil materiales de lectura para complementar tu educación fisica y virtual";
    item.foto = "foto_trujillo_sanisidro_7.jpg";
    item.video = "Upbk6x9ZQtk";
    item.facultad = 1;
    data.push(item);
    var item = {};
    item.ambiente = "LOSA DEPORTIVA";
    item.texto = "Desarrollamos para ti un sistema de acompañamiento remoto con programas de nivelación, orientación personal, talleres extracurriculares y charlas";
    item.facultad = 1;
    data.push(item);

    var item = {};
    item.ambiente = "LABORATORIO DE CÓMPUTO";
    //item.texto = "9 de cada 10 de nuestros egresados, consiguen trabajo en su primer año de egreso";
    //item.video = "xjeeaF9ZHKU";
    item.facultad = 1;
    data.push(item);

    var item = {};
    item.ambiente = "AULA";
    item.texto = "";
    item.foto = "foto_aula.jpg";
    item.facultad = 1;
    data.push(item);

    var item = {};
    item.ambiente = "LABORATORIO DE FÍSICA";
    item.carreras = "Ingeniería Industrial,Ingeniería Civil,Ingeniería Ambiental,Ingeniería Empresarial,Ingeniería de Minas,Ingeniería Geológica,Ingeniería en Sistemas Computacionales,Ingeniería Agroindustrial";
    item.facultad = 2;
    item.facultad_label = "INGENIERÍA";
    item.texto = "Nuestros laboratorios cuentan con equipos digitales, sensores de medición y elementos computarizados que permiten el análisis de los fenómenos físicos de forma práctica y dando una gran experiencia enseñanza-aprendizaje.";
    item.foto = "foto_trujillo_sanisidro_9.jpg";
    data.push(item);
    var item = {};
    item.ambiente = "LABORATORIO DE QUÍMICA";
    item.facultad = 2;
    item.carreras = "Ingeniería Industrial,Ingeniería Civil,Ingeniería Ambiental,Ingeniería Empresarial,Ingeniería de Minas,Ingeniería Geológica";
    item.texto = "Este laboratorio cuenta con los equipos de seguridad necesarios para poder trabajar con reactivos químicos, como campanas de gases, duchas y lava ojos de emergencia. Además contamos con los equipos necesarios para el desarrollo de nuestras prácticas experimentales.";
    item.foto = "foto_trujillo_sanisidro_10.jpg";
    data.push(item);

    var item = {};
    item.ambiente = "LABORATORIO DE MECÁNICA DE ROCAS";
    item.facultad = 2;
    item.carreras = "Ingeniería de Minas,Ingeniería Geológica,Ingeniería Civil";
    item.foto = "foto_trujillo_sanisidro_11.jpg";
    item.texto = "";
    data.push(item);

    var item = {};
    item.ambiente = "LABORATORIO DE ELECTRÓNICA";
    item.facultad = 2;
    item.carreras = "Ingeniería Electrónica,Ingeniería Mecatrónica, Ingeniería de Sistemas Computacionales";
    item.foto = "foto_trujillo_sanisidro_12.jpg";
    item.texto = "Este ambiente cuenta con equipos de medición electrónica  para realizar prácticas de circuitos, como Osciloscopios, Generadores de ondas, Multímetros, y componentes electrónicos diversos.";
    data.push(item);
    var item = {};
    item.ambiente = "LABORATORIO DE AUTOMATIZACIÓN";
    item.facultad = 2;
    item.carreras = "Ingeniería Industrial,Ingeniería Electrónica";
    item.foto = "foto_trujillo_sanisidro_13.jpg";
    item.texto = "Este ambiente cuenta con equipos de automatización electroneumática y con controladores lógicos donde podemos trabajar programación industrial. Utilizamos software de marcas reconocida a nivel mundial que permiten complementar la instrucción teórica.";
    data.push(item);
    var item = {};
    item.ambiente = "LABORATORIO DE ANÁLISIS INSTRUMENTAL";
    item.facultad = 2;
    item.carreras = "Ingeniería Ambiental,Ingeniería Geológica,Ingeniería de Minas,Ingeniería Agroindustrial";
    item.foto = "foto_trujillo_sanisidro_14.jpg";
    item.texto = "En este laboratorio vemos los conceptos principales del análisis instrumental en química analítica y bioanalítica. Enfatiza el uso de instrumentación comercial moderna y los equipos de seguridad como soporte principal.";
    data.push(item);
    var item = {};
    item.ambiente = "LABORATORIO DE CONCRETO Y ESTRUCTURAS";
    item.facultad = 2;
    item.carreras = "Ingeniería Civil";
    item.texto = "Este laboratorio nos permite estudiar la resistencia del concreto y otros materiales, como ladrillo, madera, acero, etc. para su correcto empleo en obras.";
    item.foto = "foto_trujillo_sanisidro_15.jpg";
    data.push(item);
    var item = {};
    item.ambiente = "LABORATORIO DE HIDRÁULICA";
    item.foto = "foto_trujillo_sanisidro_16.jpg";
    item.facultad = 2;
    item.carreras = "Ingeniería Civil,Ingeniería de Minas,Ingeniería Ambiental";
    item.texto = "Este laboratorio nos permite estudiar el comportamiento y propiedades del agua para trabajar en proyectos de obras hidráulicas.";
    data.push(item);
    var item = {};
    item.ambiente = "LABORATORIO DE MECÁNICA DE SUELOS Y PAVIMENTOS";
    item.foto = "foto_trujillo_sanisidro_17.jpg";
    item.facultad = 2;
    item.carreras = "Ingeniería Civil";
    item.texto = "En este laboratorio podemos estudiar el comportamiento físico y mecánico de los suelos para conocer  su resistencia como elemento de soporte de las construcciones.";
    data.push(item);

    var item = {};
    item.ambiente = "GABINETE DE TOPOGRAFÍA MINERA";
    item.foto = "foto_minas.jpg";
    item.facultad = 2;
    item.carreras = "Ingeniería de Minas";
    item.texto = "Contamos con un gabinete donde guardamos los equipos topográficos para realizar practicas en la universidad o en campo.";
    data.push(item);

    var item = {};
    item.ambiente = "LABORATORIO DE MICROBIOLOGÍA Y BIOLOGÍA";
    item.facultad_label = "SALUD"
    item.facultad = 3;
    item.carreras = "Ingeniería Ambiental,Ingeniería Agroindustrial,Enfermería,Obstetricia,Nutrición y Dietética,Terapia Física y Rehabilitación";

    item.texto = "Estos ambientes cuentan con elementos para el análisis y cuidado de cultivos y elementos biológicos; así como la digitalización en video mediante las interfases y cámaras para que todo pueda ser observado y revisado al detalle.";
    data.push(item);



    var item = {};
    item.ambiente = "TALLER DE COCINA";
    item.foto = "foto_trujillo_sanisidro_19.jpg";
    item.texto = "Aquí contamos con las herramientas y equipos necesarios para desarrollar técnicas culinarias, cocina nacional e internacional, panadería, pastelería, entre otros.";
    item.facultad = 4;
    item.carreras = "Gastronomía y Gestión de Restaurantes,Administración y Servicios Turísticos";
    item.facultad_label = "NEGOCIOS"
    data.push(item);

    var item = {};
    item.ambiente = "TALLER DE HOUSEKEEPING";
    item.texto = "Este taller recrea la habitación de un hotel con todos los implementos necesarios, y en ella podremos practicar distintas actividades del área de Housekeeping.";
    item.carreras = "Administración y Servicios Turísticos";
    item.facultad = 4;
    data.push(item);


    for(var i=0;i<data.length;i++){ data[i].id = i+1; }
      return (<Campus titulo="Trujillo - San Isidro" folder="trujillo_sanisidro" fotoname="TRU_SI" data={data} nro_mapa={6}></Campus>);
  }
}
export default Trujillo_sanisidro;
