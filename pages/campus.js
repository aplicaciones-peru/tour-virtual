import Head from 'next/head'
import Link from 'next/link'
import Layout from '../components/layout'
//<Link href="/campus"><a>campus</a></Link>
import styles from './home.module.css'
/*



   */
class Campus extends React.Component {
  constructor(props) {
    super(props);
    this.state = {showCampus:true}
  }
  onOpenCampus(){
    this.setState({showCampus:true});
  }
  onCloseCampus(){
    this.setState({showCampus:false});
  }
  render() {
    return (<Layout>
        <div className={styles.bloque_campus}>
          <div className={styles.content}>
            <Link href="/">
            <div className={styles.yellowClose}></div>
            </Link>
            <div className={styles.titulo}>
                 <div>Nuestros</div>
                 <div className={styles.campus}>CAMPUS</div>
            </div>
            <div>
                <div className={styles.content_thumbs}>
                 <Link href="/campus/brena">
                  <a className={styles.thumbs}>
                  <div className={styles.thumbs_image}>
                    <img src="/img/c1.png"/>
                  </div>
                  <div className={styles.label}><span className={styles.icoLeft}>> </span>Breña</div>
                  </a>
                 </Link>

                  <Link href="/campus/chorrillos">
                  <a className={styles.thumbs}>
                  <div className={styles.thumbs_image}><img src="/img/c2.png"/></div>
                  <div className={styles.label}><span className={styles.icoLeft}>> </span>Chorrillos</div>
                  </a>
                  </Link>

                   <Link href="/campus/comas">
                  <a className={styles.thumbs}>
                 <div className={styles.thumbs_image}><img src="/img/c3.png"/></div>
                 <div className={styles.label}><span className={styles.icoLeft}>> </span>Comas</div>
                 </a></Link>

                 <Link href="/campus/losolivos">
                  <a className={styles.thumbs}>
                 <div className={styles.thumbs_image}><img src="/img/c4.png"/></div>
                 <div className={styles.label}><span className={styles.icoLeft}>> </span>Los Olivos</div>
                 </a></Link>

                 <Link href="/campus/sanjuandelurigancho">
                  <a className={styles.thumbs}>
                 <div className={styles.thumbs_image}><img src="/img/c5.png"/></div>
                 <div className={styles.label}><span className={styles.icoLeft}>> </span>San Juan de Lurigancho</div>
                 </a></Link>

                 <Link href="/campus/trujillo_elmolino">
                  <a className={styles.thumbs}>
                 <div className={styles.thumbs_image}><img src="/img/c6.png"/></div>
                 <div className={styles.label}><span className={styles.icoLeft}>> </span>Trujillo - El Molino</div>
                 </a></Link>

                 <Link href="/campus/trujillo_sanisidro">
                  <a className={styles.thumbs}>
                 <div className={styles.thumbs_image}><img src="/img/c7.png"/></div>
                 <div className={styles.label}><span className={styles.icoLeft}>> </span>Trujillo - San Isidro</div>
                 </a></Link>

                 <Link href="/campus/cajamarca">
                  <a className={styles.thumbs}>
                 <div className={styles.thumbs_image}><img src="/img/c8.png"/></div>
                 <div className={styles.label}><span className={styles.icoLeft}>> </span>Cajamarca</div>
                 </a></Link>

               </div>
            </div>
          </div>
        </div>
      </Layout>);
  }
}
export default Campus;
