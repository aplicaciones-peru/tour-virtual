import Head from 'next/head'
import Link from 'next/link'
import Layout from '../components/layout'
//<Link href="/campus"><a>campus</a></Link>
import styles from './home.module.css'
//const
class Home extends React.Component {

  constructor(props) {
    super(props);
    this.onPressSound = this.onPressSound.bind( this );
    this.audio = React.createRef();
    this.state={sound:false}
  }
  componentDidMount(){
    //audioTune.load();

  }
  onPressSound(){
    if(!this.state.sound){
      this.setState({sound:true});
      this.refs.audio.currentTime=0;
      this.refs.audio.play();
    }else{
      this.setState({sound:false});
      this.refs.audio.pause();
    }

  }
  onPressTourVirtual(){

  }
  render() {
    return (<Layout>
      <audio ref="audio" preload="yes">
        <source src="sound/locucion.mpeg" type="audio/mpeg"/>
      </audio>
        <div className={styles.bloque_bienvenida_content}>
        <div className={styles.bloque_bienvenida}>
          <div className={styles.bienvenido}>Bienvenido a nuestro</div>
          <Link href="/campus">
          <div className={styles.tour_virtual}>Tour Virtual</div>
          </Link>

          <div className={styles.tooltip_left}>
          <div className={styles.tour_instrucciones} onClick={this.onPressSound}>
            <div className={styles.icoSound}></div>
            <div className={styles.icoSoundLabel} >Clic aquí para escuchar instrucciones</div>
          </div>
          <span className={styles.tiptext}>Escuchar instrucciones</span>
          </div>
          <div className={styles.iconos_ini}>
            <Link href="/campus">
            <div className={styles.tooltip_left}>
            <div className={styles.icono+' '+styles.campus}><div></div></div>
            <span className={styles.tiptext}>Campus</span>
            </div>
            </Link>
            <div className={styles.tooltip_left}>
            <a href="https://www.facebook.com/UPN/" target="_blank">
              <div className={styles.icono+' '+styles.fb}><div></div></div>
            </a>
            <span className={styles.tiptext}>Facebook</span>
            </div>
            <div className={styles.tooltip_left}>
            <a href="https://instagram.com/upn?igshid=en91buw7iax5" target="_blank">
                <div className={styles.icono+' '+styles.ig}><div></div></div>
            </a>
            <span className={styles.tiptext}>Instragram</span>
            </div>

          </div>
        </div>
        </div>
      </Layout>);
  }
}
export default Home;
